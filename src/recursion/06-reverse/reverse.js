/**
 * @param {string} s
 * @returns {string}
 */
const reverse = (s) => {
  if (s.length === 1) return s[0];
  return s[s.length - 1] + reverse(s.substring(0, s.length - 1));
};

module.exports = reverse;
