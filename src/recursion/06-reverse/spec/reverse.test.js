const expect = require('chai').expect;
const reverse = require('../reverse');

describe('reverse', () => {
  it('should return "oof" when s = "foo"', () => {
    expect(reverse('foo')).to.equal('oof');
  });

  it('should return "gruobmexuL" when s = "Luxembourg"', () => {
    expect(reverse('Luxembourg')).to.equal('gruobmexuL');
  });

  it('should return "sbireraps krop tsaor" when s = "roast pork spareribs"', () => {
    expect(reverse('roast pork spareribs')).to.equal('sbireraps krop tsaor');
  });
});
