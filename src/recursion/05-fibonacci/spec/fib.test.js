const expect = require('chai').expect;
const fib = require('../fib');

describe('fib', () => {
  it('should return 3 if n = 4', () => {
    expect(fib(4)).to.equal(3);
  });

  it('should return 8 if n = 6', () => {
    expect(fib(6)).to.equal(8);
  });

  it('should return 34 if n = 9', () => {
    expect(fib(9)).to.equal(34);
  });
});
