/**
 *
 * @param {number} n
 * @returns {number}
 */
const recursiveRange = (n) => {
  if (n === 1) return n;

  return n + recursiveRange(n - 1);
};

module.exports = recursiveRange;
