const expect = require('chai').expect;
const recursiveRange = require('../recursiveRange');

describe('recursiveRange', () => {
  it('should return 10 when n = 4', () => {
    expect(recursiveRange(4)).to.equal(10);
  });

  it('should return 36 when n = 8', () => {
    expect(recursiveRange(8)).to.equal(36);
  });
});
