/**
 *
 * @param {string} s
 * @returns {boolean}
 */
const isPalindrome = (s, left, right) => {
  if (left >= right) return true;
  if (s[left] !== s[right - 1]) return false;
  return isPalindrome(s, left + 1, right - 1);
};

module.exports = isPalindrome;
