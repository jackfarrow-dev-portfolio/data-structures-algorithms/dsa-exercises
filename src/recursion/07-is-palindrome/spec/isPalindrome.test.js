const expect = require('chai').expect;
const isPalindrome = require('../isPalindrome');

describe('isPalindrome', () => {
  it('should return true if s = "oto"', () => {
    const s = 'oto';
    expect(isPalindrome(s, 0, s.length)).to.equal(true);
  });
  it('should return true if s = "racecar"', () => {
    const s = 'racecar';
    expect(isPalindrome(s, 0, s.length)).to.equal(true);
  });

  it('should return false if s = "bluegrass"', () => {
    const s = 'bluegrass';
    expect(isPalindrome('bluegrass', 0, s.length)).to.equal(false);
  });
});
