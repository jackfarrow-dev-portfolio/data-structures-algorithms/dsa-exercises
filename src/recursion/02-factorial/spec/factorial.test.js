const expect = require('chai').expect;
const factorial = require('../factorial');

describe('factorial', () => {
  it('should return 24 when n = 4', () => {
    expect(factorial(4)).to.equal(24);
  });

  it('should return 120 when n = 5', () => {
    expect(factorial(5)).to.equal(120);
  });

  it('should return 720 when n = 6', () => {
    expect(factorial(6)).to.equal(720);
  });
});
