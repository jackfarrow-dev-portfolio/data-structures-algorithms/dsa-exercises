const expect = require('chai').expect;
const power = require('../power');

describe('power', () => {
  it('should return 100 when base is 10 and exp is 2', () => {
    expect(power(10, 2)).to.equal(100);
  });

  it('should return 16 when base is 2 and exp is 4', () => {
    expect(power(2, 4)).to.equal(16);
  });

  it('should return 1000 when base is 1000 and exp is 1', () => {
    expect(power(1000, 1)).to.equal(1000);
  });

  it('should return 1 when base is 10 and exp is 0', () => {
    expect(power(10, 0)).to.equal(1);
  });
});
