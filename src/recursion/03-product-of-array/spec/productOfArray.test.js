const expect = require('chai').expect;
const productOfArray = require('../productOfArray');

describe('productOfArray', () => {
  it('should return 1050 when nums = [3, 7, 10, 5]', () => {
    expect(productOfArray([3, 7, 10, 5])).to.equal(1050);
  });

  it('should return 5040 when nums = [1, 2, 3, 4, 5, 6, 7]', () => {
    expect(productOfArray([1, 2, 3, 4, 5, 6, 7])).to.equal(5040);
  });
});
