/**
 *
 * @param {number[]} nums
 * @return {number}
 */
const productOfArray = (nums) => {
  if (nums.length === 1) return nums[0];

  let current = nums.pop();
  return current * productOfArray(nums);
};

module.exports = productOfArray;
