/**
 *
 * @param {number[][]} grid
 * @returns {number}
 */
const numIslands = (grid) => {
  let answer = 0;
  const m = grid.length;
  const n = grid[0].length;
  const seen = new Array(m);
  for (let i = 0; i < m; i++) {
    seen[i] = new Array(n).fill(false);
  }

  const directions = [
    [-1, 0],
    [0, -1],
    [0, 1],
    [1, 0],
  ];

  const isValid = (row, col) =>
    0 <= row && row < m && 0 <= col && col < n && grid[row][col] === 1;

  const dfs = (row, col) => {
    for (const [dx, dy] of directions) {
      let nextRow = row + dy;
      let nextCol = col + dx;
      if (isValid(nextRow, nextCol) && !seen[nextRow][nextCol]) {
        seen[nextRow][nextCol] = true;
        dfs(nextRow, nextCol);
      }
    }
  };

  for (let i = 0; i < m; i++) {
    for (let j = 0; j < n; j++) {
      if (isValid(i, j) && !seen[i][j]) {
        seen[i][j] = true;
        answer++;
        dfs(i, j);
      }
    }
  }
  return answer;
};

module.exports = numIslands;
