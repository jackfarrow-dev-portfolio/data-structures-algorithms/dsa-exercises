/**
 *
 * @param {number} n
 * @param {number[][]} edges
 * @param {number} source
 * @param {number} destination
 */
const findPathIfExists = (n, edges, source, destination) => {
  if (!edges.length) return false;
  const seen = new Array(n).fill(false);

  const buildGraph = () => {
    const graph = new Map();
    for (let i = 0; i < n; i++) {
      graph.set(i, []);
    }

    for (const edge of edges) {
      const [x, y] = edge;
      graph.get(x).push(y);
      graph.get(y).push(x);
    }
    return graph;
  };

  let answer = false;

  const graph = buildGraph();

  for (let i = source + 1; i < n; i++) {
    const node = graph.get(i);
    if (node.indexOf(source) === -1) {
      seen[i] = true;
    }
  }

  const dfs = (nodeIndex) => {
    const node = graph.get(nodeIndex);
    for (const neighbor of node) {
      if (neighbor === destination) {
        answer = true;
        return;
      }
      if (!seen[neighbor]) {
        seen[neighbor] = true;
        dfs(neighbor);
      }
    }
  };

  for (let i = source; i < destination; i++) {
    if (!seen[i]) {
      seen[i] = true;
      dfs(i);
    }
  }

  return answer;
};

module.exports = findPathIfExists;
