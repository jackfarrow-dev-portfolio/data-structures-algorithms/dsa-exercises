const expect = require('chai').expect;
const findPathIfExists = require('../findPathIfExists');

describe('findPathIfExists', () => {
  it('should return true if n = 3, edges =  [[0, 1], [1, 2], [2, 0]], source = 0 and destination = 2', () => {
    expect(
      findPathIfExists(
        3,
        [
          [0, 1],
          [1, 2],
          [2, 0],
        ],
        0,
        2
      )
    ).to.be.true;
  });

  it('should return false if n = 6, edges =   [[0,1],[0,2],[3,5],[5, 4],[4,3]], source = 0 and destination = 5', () => {
    expect(
      findPathIfExists(
        6,
        [
          [0, 1],
          [0, 2],
          [3, 5],
          [5, 4],
          [4, 3],
        ],
        0,
        5
      )
    ).to.be.false;
  });
});
