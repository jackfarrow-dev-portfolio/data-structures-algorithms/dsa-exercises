const expect = require('chai').expect;
const keysAndRooms = require('../keysAndRooms');

describe('keysAndRooms', () => {
  it('should return true when rooms = [[1],[2],[3],[]]', () => {
    expect(keysAndRooms([[1], [2], [3], []])).to.be.true;
  });

  it('should return false when rooms = [[1,3],[3,0,1],[2],[0]]', () => {
    expect(keysAndRooms([[1, 3], [3, 0, 1], [2], [0]])).to.be.false;
  });

  it('should return false when rooms = [[4],[3],[],[2,5,7],[1],[],[8,9],[],[],[6]]', () => {
    expect(
      keysAndRooms([[4], [3], [], [2, 5, 7], [1], [], [8, 9], [], [], [6]])
    ).to.be.false;
  });
});
