/**
 *
 * @param {number[][]} rooms
 * @returns {boolean}
 */
const keysAndRooms = (rooms) => {
  const seen = new Set();
  seen.add(0);

  const dfs = (nodeIndex) => {
    for (const neighbor of rooms[nodeIndex]) {
      if (!seen.has(neighbor)) {
        seen.add(neighbor);
        dfs(neighbor);
      }
    }
  };

  dfs(0);
  return seen.size == rooms.length;
};
// const keysAndRooms = (rooms) => {
//   // THIS SOLUTION DOES NOT WORK
//   const n = rooms.length;
//   const seen = new Map();

//   const buildGraph = () => {
//     const graph = new Map();
//     for (let i = 0; i < n; i++) {
//       graph.set(i, []);
//       seen.set(i, false);
//       const edges = rooms[i];
//       for (const edge of edges) {
//         if (edge !== i) {
//           graph.get(i).push(edge);
//         }
//       }
//     }
//     return graph;
//   };

//   const graph = buildGraph();
//   console.log(graph); // OK
//   //   console.log(seen);

//   const dfs = (nodeIndex) => {
//     const node = graph.get(nodeIndex);
//     for (const neighbor of node) {
//       // console.log(`neighbor: ${neighbor}`);
//       if (seen.get(neighbor) === false) {
//         seen.set(neighbor, true);
//         dfs(neighbor);
//       }
//     }
//   };

//   seen.set(0, true);

//   for (let i = 0; i < n; i++) {
//     dfs(i);
//   }

//   console.log('seen (end):');
//   console.log(seen);

//   let answer = true;
//   seen.forEach((val, key) => {
//     if (val === false) {
//       answer = val;
//       return;
//     }
//   });
//   return answer;
// };

module.exports = keysAndRooms;
