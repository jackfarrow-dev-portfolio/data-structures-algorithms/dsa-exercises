# Number of Provinces

> There are `n` cities. A province is a group of directly or indirectly
> connected cities and no other cities outside of the group. You are given an
> `n x n` matrix `isConnected` where `isConnected[i][j]` = 1 if the `i`th city
> and the `j`th city are directly connected, and `isConnected[i][j]` = 0
> otherwise. Return the total number of provinces.

## Example 1:

```
        1 <--> 2
            3
```

- **Input**: `isConnected = [[1, 1, 0], [1, 1, 0], [0, 0, 1]]`
- **Outupt**: 2

## Example 2:

```
        1       2
            3
```

- **Input**: `isConnected = [[1, 0, 0], [0, 1, 0], [0, 0, 1]]`
- **Output**: 3
