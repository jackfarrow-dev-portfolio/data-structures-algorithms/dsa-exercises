/**
 *
 * @param {number[][]} isConnected
 * @returns {number}
 */
const numProvinces = (isConnected) => {
  const n = isConnected.length;
  const seen = new Array(n).fill(false);
  let answer = 0;

  const buildGraph = (matrix) => {
    const graph = new Map();
    for (let i = 0; i < n; i++) {
      graph.set(i, []);
    }

    for (let i = 0; i < n; i++) {
      for (let j = i + 1; j < n; j++) {
        if (matrix[i][j] === 1) {
          graph.get(i).push(j);
          graph.get(j).push(i);
        }
      }
    }
    return graph;
  };

  const graph = buildGraph(isConnected);

  const dfs = (nodeIndex) => {
    const node = graph.get(nodeIndex);
    for (let i = 0; i < node.length; i++) {
      let neighbor = node[i];
      if (!seen[neighbor]) {
        seen[neighbor] = true;
        dfs(neighbor);
      }
    }
  };

  for (let i = 0; i < n; i++) {
    if (!seen[i]) {
      seen[i] = true;
      answer++;
      dfs(i);
    }
  }
  return answer;
};

module.exports = numProvinces;
