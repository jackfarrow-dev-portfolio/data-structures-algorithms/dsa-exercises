const expect = require('chai').expect;
const numProvinces = require('../numProvinces');

describe('numProvinces', () => {
  it('should return 2 when isConnected = [[1, 1, 0], [1, 1, 0], [0, 0, 1]]', () => {
    const isConnected = [
      [1, 1, 0],
      [1, 1, 0],
      [0, 0, 1],
    ];
    expect(numProvinces(isConnected)).to.equal(2);
  });

  it('should return 3 when isConnected = [[1, 0, 0], [0, 1, 0], [0, 0, 1]]', () => {
    const isConnected = [
      [1, 0, 0],
      [0, 1, 0],
      [0, 0, 1],
    ];
    expect(numProvinces(isConnected)).to.equal(3);
  });
});
