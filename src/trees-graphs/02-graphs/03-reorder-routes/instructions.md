# Reorder Routes to Make All Paths Lead to the City Zero

> There are `n` cities numbered from `0` to `n-1` and `n-1` roads such that
> there is only one way to travel between two different cities. Roads are
> represented by `connections`, where `connections[i] = [x, y]` represents a
> road from city `x` to city `y`. The edges are directed. You need to swap the
> direction of some edges so that every city can reach city `0`. Return the
> minimum number of swaps needed.

## Example 1:

```
0 --> 1 --> 3 <-- 2
^
|__ 4 --> 5

```

- **Input**: `n = 6`, `connections =`

```
[
    [0, 1],
    [1, 3],
    [2, 3],
    [4, 0],
    [4, 5]
]
```

- **Output**: 3
- **Explanation**: Change the direction of the edges between 0 and 1, 1 and 3,
  and 4 and 5 such that each node can reach the node 0.

## Solution Explanation

Another way to look at this problem statment is: "How many roads (edges) are
pointing **away** from 0?" To find the solution, we can:

1. Create a `Set` that stores the edges **as they are**

2. Create a `Map` **_as if the graph were undirected_**, such that `graph[i]` =
   `[all nodes connected to i]`

3. Do a `dfs` starting from 0, moving _away_ from 0, on the undirected graph.
   Record the traversal edge, and **compare each traversal edge** to the edge
   stored in the Set (which corresponds to the original edge). If there is a
   **match**, we know that this edge must be **reversed**.
