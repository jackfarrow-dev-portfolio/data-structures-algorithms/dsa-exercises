/**
 *
 * @param {number} n
 * @param {number[][]} connections
 */
const reorderRoutes = (n, connections) => {
  // Create a set to hold the edges as-is
  const set = new Set();

  const seen = new Array(n).fill(false);

  let answer = 0;

  const convertToHash = (row, col) => `${row}, ${col}`;

  // Build an UNDIRECTED graph of nodes
  const buildGraph = () => {
    const graph = new Map();
    for (let i = 0; i < n; i++) {
      graph.set(i, []);
    }

    for (const [x, y] of connections) {
      graph.get(x).push(y);
      graph.get(y).push(x);
      // Add each edge to set (converted to a string)
      set.add(convertToHash(x, y));
    }
    return graph;
  };

  const graph = buildGraph();

  const dfs = (nodeIndex) => {
    const node = graph.get(nodeIndex);
    for (let i = 0; i < node.length; i++) {
      let neighbor = node[i];
      if (!seen[neighbor]) {
        // If the [node, neighbor] combination is present in set,
        // we know we have an edge that needs to be reversed
        if (set.has(convertToHash(nodeIndex, neighbor))) {
          answer++;
        }
        seen[neighbor] = true;
        dfs(neighbor);
      }
    }
  };

  for (let i = 0; i < n; i++) {
    if (!seen[i]) {
      seen[i] = true;
      dfs(i);
    }
  }

  return answer;
};

module.exports = reorderRoutes;
