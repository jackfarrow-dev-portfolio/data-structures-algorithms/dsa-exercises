const expect = require('chai').expect;
const reorderRoutes = require('../reorderRoutes');

describe('reorderRoutes', () => {
  it(`should return 3 if n = 6 and connections = [
    [0, 1],
    [1, 3],
    [2, 3],
    [4, 0],
    [4, 5]
]`, () => {
    expect(
      reorderRoutes(6, [
        [0, 1],
        [1, 3],
        [2, 3],
        [4, 0],
        [4, 5],
      ])
    ).to.equal(3);
  });
});
