const TreeNode = require('../TreeNode');

/**
 *
 * @param {TreeNode} root
 * @returns {number}
 */
const deepestLeavesSum = (root) => {
  if (!root) return 0;
  // let sums = [];
  let queue = [root];

  while (queue.length) {
    let nodesInRow = queue.length;
    let nextQueue = [];
    let currentSum = 0;
    for (let i = 0; i < nodesInRow; i++) {
      let node = queue[i];
      currentSum += node.val;

      if (node.left) nextQueue.push(node.left);
      if (node.right) nextQueue.push(node.right);
    }
    // sums.push(currentSum);
    sum = currentSum;
    queue = nextQueue;
  }
  // return sums[sums.length - 1];
  return sum;
};

module.exports = deepestLeavesSum;
