const expect = require('chai').expect;
const TreeNode = require('../../TreeNode');
const deepestLeavesSum = require('../deepestLeavesSum');

describe('deepestLeavesSum', () => {
  it('should return 15 when root = [1, 2, 3, 4, 5, null, 6, 7, null, null, null, null, 8]', () => {
    const seven = new TreeNode(7);
    const eight = new TreeNode(8);
    const four = new TreeNode(4, null, seven);
    const five = new TreeNode(5);
    const six = new TreeNode(6, null, eight);
    const two = new TreeNode(2, four, five);
    const three = new TreeNode(3, null, six);
    const root = new TreeNode(1, two, three);

    expect(deepestLeavesSum(root)).to.equal(15);
  });

  it('should return 19 when root = [6, 7, 8, 2, 7, 1, 3, 9, 1, 4, null, null, null, 5]', () => {
    const nine = new TreeNode(9);
    const one = new TreeNode(1);
    const four = new TreeNode(4);
    const five = new TreeNode(5);
    const two = new TreeNode(2, nine);
    const seven = new TreeNode(7, one, four);
    const oneB = new TreeNode(1);
    const three = new TreeNode(3, null, five);
    const sevenB = new TreeNode(7, two, seven);
    const eight = new TreeNode(8, oneB, three);
    const root = new TreeNode(6, sevenB, eight);

    expect(deepestLeavesSum(root)).to.equal(19);
  });
});
