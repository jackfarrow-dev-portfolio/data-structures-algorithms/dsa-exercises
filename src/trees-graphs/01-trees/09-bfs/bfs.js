const TreeNode = require('../TreeNode');
/**
 *
 * @param {TreeNode} root
 * @returns {void}
 */
const bfs = (root) => {
  let queue = [root];

  while (queue.length) {
    let qLen = queue.length;
    let nextQueue = [];

    for (let i = 0; i < qLen; i++) {
      // let node = queue.shift();
      let node = queue[i];
      if (node.left) nextQueue.push(node.left);
      if (node.right) nextQueue.push(node.right);
    }
    queue = nextQueue;
  }
};

/*
                    1
            2               3
        4       5       6       7

*/

const four = new TreeNode(4);
const five = new TreeNode(5);
const six = new TreeNode(6);
const seven = new TreeNode(7);
const two = new TreeNode(2, four, five);
const three = new TreeNode(3, six, seven);
const root = new TreeNode(1, two, three);

bfs(root); // 1 2 3 4 5 6 7
