const expect = require('chai').expect;
const validateBst = require('../validateBst');
const TreeNode = require('../../TreeNode');

describe('validateBst', () => {
  it('should return true when root = [2, 1, 3]', () => {
    const one = new TreeNode(1);
    const three = new TreeNode(3);
    const root = new TreeNode(2, one, three);
    expect(validateBst(root)).to.be.true;
  });

  it('should return false when root = [5, 1, 4, null, null, 3, 6]', () => {
    const three = new TreeNode(3);
    const six = new TreeNode(6);
    const one = new TreeNode(1);
    const four = new TreeNode(4, three, six);
    const root = new TreeNode(5, one, four);

    expect(validateBst(root)).to.be.false;
  });

  it('should return false when root = [2, 2, 2]', () => {
    const right = new TreeNode(2);
    const left = new TreeNode(2);
    const root = new TreeNode(2, left, right);
    expect(validateBst(root)).to.be.false;
  });

  it('should return false when root = [5,4,6,null,null,3,7]', () => {
    /*
                              5
                      4               6
                                  3       7
          */
    const three = new TreeNode(3);
    const seven = new TreeNode(7);
    const four = new TreeNode(4);
    const six = new TreeNode(6, three, seven);
    const root = new TreeNode(5, four, six);
    expect(validateBst(root)).to.be.false;
  });
});
