const TreeNode = require('../TreeNode');

/**
 *
 * @param {TreeNode} root
 * @returns {boolean}
 */
const validateBst = (root) => {
  const helper = (node, low, high) => {
    if (!node) return true;

    if (node.val <= low || node.val >= high) {
      return false;
    }

    let left = helper(node.left, low, node.val);
    let right = helper(node.right, node.val, high);
    return left && right;
  };
  return helper(root, -Infinity, Infinity);
};
// const validateBst = (root) => {
//   if (!root) return true;

//   const helper = (node, max, min) => {
//     if (!node) return true;
//     // console.log(`min: ${min} <--> nodeVal: ${node.val} <--> max: ${max}`);

//     let left = true;
//     let right = true;
//     if (node.left) {
//       if (node.left.val <= min) return false;
//       min = Math.min(min, node.left.val);
//       left = helper(node.left, max, min);
//     }
//     if (node.right) {
//       if (node.right.val >= max) return false;
//       max = Math.max(max, node.right.val);
//       right = helper(node.right, max, min);
//     }
//     return left && right;
//   };
//   return helper(root, -Infinity, Infinity);
// };

// const validateBst = (root) => {
//   if (!root) return true;
//   let leftVal = root.left ? root.left.val : Infinity;
//   let rightVal = root.right ? root.right.val : -Infinity;
//   if ((leftVal && leftVal >= root.val) || (rightVal && rightVal <= root.val)) {
//     return false;
//   }

//   let left = validateBst(root.left);
//   let right = validateBst(root.right);
//   return left && right;
// };

module.exports = validateBst;
