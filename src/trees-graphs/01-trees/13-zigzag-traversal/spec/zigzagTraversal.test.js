const expect = require('chai').expect;
const TreeNode = require('../../TreeNode');
const zigzagTraversal = require('../zigzagTraversal');

describe('zigzagTraversal', () => {
  it('should return [[3], [20, 9], [15, 7]] when root = [3, 8, 20, null, null, 15, 7]', () => {
    const fifteen = new TreeNode(15);
    const seven = new TreeNode(7);
    const nine = new TreeNode(9);
    const twenty = new TreeNode(20, fifteen, seven);
    const root = new TreeNode(3, nine, twenty);

    expect(zigzagTraversal(root)[0][0]).to.equal(3);
    expect(zigzagTraversal(root)[1][0]).to.equal(20);
    expect(zigzagTraversal(root)[1][1]).to.equal(9);
    expect(zigzagTraversal(root)[2][0]).to.equal(15);
    expect(zigzagTraversal(root)[2][1]).to.equal(7);
  });

  it('should return [[1]] when root = [1]', () => {
    expect(zigzagTraversal(new TreeNode(1))[0])
      .to.be.an('array')
      .that.contains(1);
  });

  it('should return [] when root = null', () => {
    expect(zigzagTraversal(null).length).to.equal(0);
  });

  it('should return [[1],[3,2],[4,5]] when root = [1,2,3,4,null,null,5]', () => {
    /*
                              1
                      2               3
                  4                       5
          */
    const four = new TreeNode(4);
    const five = new TreeNode(5);
    const two = new TreeNode(2, four);
    const three = new TreeNode(3, null, five);
    const root = new TreeNode(1, two, three);

    expect(zigzagTraversal(root)[0][0]).to.equal(1);
    expect(zigzagTraversal(root)[1][0]).to.equal(3);
    expect(zigzagTraversal(root)[1][1]).to.equal(2);
    expect(zigzagTraversal(root)[2][0]).to.equal(4);
    expect(zigzagTraversal(root)[2][1]).to.equal(5);

    expect(zigzagTraversal(root)[0][0]).to.equal(1);
  });
});
