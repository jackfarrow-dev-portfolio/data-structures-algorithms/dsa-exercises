const TreeNode = require('../TreeNode');

/**
 *
 * @param {TreeNode} root
 * @returns {number[][]}
 */
const zigzagTraversal = (root) => {
  if (!root) return [];
  let queue = [root];
  const result = [];
  let startFromLeft = true;
  let nodeVals;

  while (queue.length) {
    let nodesInRowCount = queue.length;
    let nextQueue = [];
    nodeVals = [];

    for (let i = 0; i < nodesInRowCount; i++) {
      let node = queue[i];
      if (startFromLeft) nodeVals.push(node.val);
      else nodeVals.unshift(node.val);
      if (node.left) nextQueue.push(node.left);
      if (node.right) nextQueue.push(node.right);
    }
    queue = nextQueue;
    result.push(nodeVals);
    startFromLeft = !startFromLeft;
  }

  return result;
};

module.exports = zigzagTraversal;
