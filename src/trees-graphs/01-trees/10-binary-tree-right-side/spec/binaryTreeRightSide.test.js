const expect = require('chai').expect;
const TreeNode = require('../../TreeNode');
const binaryTreeRightSide = require('../binaryTreeRightSide');

describe('binaryTreeRightSide', () => {
  it('should return [1, 3, 4] when root = [1, 2, 3, null, 5, null, 4]', () => {
    const five = new TreeNode(5);
    const four = new TreeNode(4);
    const two = new TreeNode(2, null, five);
    const three = new TreeNode(3, null, four);
    const root = new TreeNode(1, two, three);

    expect(binaryTreeRightSide(root)).to.be.an('array').that.includes(1);
    expect(binaryTreeRightSide(root)).to.be.an('array').that.includes(3);
    expect(binaryTreeRightSide(root)).to.be.an('array').that.includes(4);
  });

  it('should return [1, 3] when root = [1, null, 3]', () => {
    const three = new TreeNode(3);
    const root = new TreeNode(1, null, three);
    expect(binaryTreeRightSide(root)).to.be.an('array').that.includes(1);
    expect(binaryTreeRightSide(root)).to.be.an('array').that.includes(3);
  });

  it('should return [] when root = []', () => {
    expect(binaryTreeRightSide(null).length).to.equal(0);
  });
});
