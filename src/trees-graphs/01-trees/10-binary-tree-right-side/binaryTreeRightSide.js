/**
 * @param {TreeNode} root
 * @returns {number[]}
 */
const binaryTreeRightSide = (root) => {
  const result = [];
  if (!root) return result;
  let queue = [root];

  while (queue.length) {
    let nodesInRow = queue.length;
    let nextQueue = [];

    for (let i = 0; i < nodesInRow; i++) {
      let node = queue[i];
      if (i === nodesInRow - 1) {
        result.push(node.val);
      }

      if (node.left) nextQueue.push(node.left);
      if (node.right) nextQueue.push(node.right);
    }
    queue = nextQueue;
  }
  return result;
};

module.exports = binaryTreeRightSide;
