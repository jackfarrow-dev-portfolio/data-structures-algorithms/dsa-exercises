const expect = require('chai').expect;
const convertArrToBst = require('../convertArrToBst');
const TreeNode = require('../../TreeNode');

describe('convertArrToBst', () => {
  const verify = (root, expected) => {
    const limit = expected.length;

    const traverse = (node) => {
      let queue = [node];
      let current = 0;
      while (queue.length && current < limit) {
        let nextQueue = [];
        let qLen = queue.length;
        for (let i = 0; i < qLen; i++) {
          let n = queue[i];
          if (n.val !== expected[current]) return false;
          if (n.left) nextQueue.push(node.left);
          if (n.right) nextQueue.push(node.right);
        }
        queue = nextQueue;
        current++;
      }
    };
    traverse(root);
    return true;
  };

  it('should return [0, -3, 9, -10, null, 5], when nums = [-10, -3, 0, 5, 9]', () => {
    const nums = [-10, -3, 0, 5, 9];
    const expected = [0, -3, 9, -10, 5];
    const root = convertArrToBst(nums);
    expect(verify(root, expected)).to.be.true;
  });

  it('should return [1, null, 3] when nums = [1, 3]', () => {
    const nums = [1, 3];
    const expected = [1, 3];
    const root = convertArrToBst(nums);
    expect(verify(root, expected)).to.be.true;
  });
});
