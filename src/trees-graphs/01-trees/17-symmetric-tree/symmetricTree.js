const TreeNode = require('../TreeNode');

/**
 *
 * @param {TreeNode} root
 * @returns {boolean}
 */
const symmetricTree = (root) => {
  const helper = (node1, node2) => {
    if (!node1 && !node2) return true;

    if (!node1 || !node2) return false;

    return (
      node1.val === node2.val &&
      helper(node1.right, node2.left) &&
      helper(node1.left, node2.right)
    );
  };
  return helper(root, root);
};
// const symmetricTree = (root) => {
//   let queue = [root];
//   let result = true;

//   const isRowSymmetric = (arr) => {
//     let left = 0;
//     let right = arr.length - 1;
//     while (left < right) {
//       if (arr[left].val !== arr[right].val) return false;
//       left++;
//       right--;
//     }
//     return true;
//   };

//   while (queue.length) {
//     let nextQueue = [];
//     let qLen = queue.length;

//     console.log(`queue:`);
//     console.log(queue);

//     result = isRowSymmetric(queue);

//     if (!result) return result;

//     for (let i = 0; i < qLen; i++) {
//       let node = queue[i];
//       if (node.left) {
//         nextQueue.push(node.left);
//       }
//       if (node.right) {
//         nextQueue.push(node.right);
//       }
//     }
//     queue = nextQueue;
//   }
//   return result;
// };

module.exports = symmetricTree;
