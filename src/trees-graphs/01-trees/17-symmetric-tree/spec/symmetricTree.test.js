const expect = require('chai').expect;
const symmetricTree = require('../symmetricTree');
const TreeNode = require('../../TreeNode');

describe('symmetricTree', () => {
  it('should return true when root = [1, 2, 2, 3, 4, 4, 3]', () => {
    const threeLeft = new TreeNode(3);
    const fourLeft = new TreeNode(4);
    const threeRight = new TreeNode(3);
    const fourRight = new TreeNode(4);
    const twoLeft = new TreeNode(2, threeLeft, fourLeft);
    const twoRight = new TreeNode(2, fourRight, threeRight);
    const root = new TreeNode(1, twoLeft, twoRight);

    expect(symmetricTree(root)).to.be.true;
  });

  it('should return false when root = [1, 2, 2, null, 3, null, 3]', () => {
    const threeLeft = new TreeNode(3);
    const threeRight = new TreeNode(3);
    const twoLeft = new TreeNode(2, null, threeLeft);
    const twoRight = new TreeNode(2, null, threeRight);
    const root = new TreeNode(1, twoLeft, twoRight);

    expect(symmetricTree(root)).to.be.false;
  });
});
