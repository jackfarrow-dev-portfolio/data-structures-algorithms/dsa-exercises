const TreeNode = require('../TreeNode');

/**
 *
 * @param {TreeNode} root
 * @returns {void}
 */
const dfsPreOrder = (root) => {
  if (!root) return;
  dfsPreOrder(root.left);
  dfsPreOrder(root.right);
  return;
};

/**
 *
 * @param {TreeNode} root
 * @returns {void}
 */
const dfsPostOrder = (root) => {
  if (!root) return;
  dfsPostOrder(root.left);
  dfsPostOrder(root.right);
};

/**
 *
 * @param {TreeNode} root
 * @returns {void}
 */
const dfsInOrder = (root) => {
  if (!root) return;
  dfsInOrder(root.left);
  dfsInOrder(root.right);
};
/*
                    1
            2               3
        4       5               6


*/

const four = new TreeNode(4);
const five = new TreeNode(5);
const six = new TreeNode(6);
const two = new TreeNode(2, four, five);
const three = new TreeNode(3, null, six);
const root = new TreeNode(1, two, three);
