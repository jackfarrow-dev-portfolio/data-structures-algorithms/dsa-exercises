# Count Good Nodes in Binary Tree

Given the `root` of a binary tree, find the number of nodes that are good. A
node is good if the path between the root and the node has no nodes with a
greater value.

## Example 1:

```
                                3
                        1               4
                    3               1       5
```

- Input: root = [3, 1, 4, 3, null, 1, 5]
- Output: 4
- Explanation:
  - Root node (3) is always a good node
  - Node 4 -> (3, 4) is the max value in the path starting from the root
  - Node 5 -> (3, 4, 5) is the max value in the path
  - Node 3 -> (3, 1, 3) is the max value in the path

## Example 2:

- Input: root = [3,3,null,4,2]
- Output: 3
- Explanation: Node 2 -> (3, 3, 2) is not good, because "3" is higher than it.

## Example 3:

- Input: root = [1]
- Output: 1
- Explanation: Root is considered as good.
