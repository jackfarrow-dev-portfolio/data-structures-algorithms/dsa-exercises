const TreeNode = require('../TreeNode');

/**
 *
 * @param {TreeNode} root
 * @returns {number}
 */
const countGoodNodes = (root) => {
  let answer = 0;

  const helper = (node, currentMax) => {
    if (!node) return 0;

    if (node.val >= currentMax) {
      answer++;
      currentMax = node.val;
    }

    left = helper(node.left, currentMax);
    right = helper(node.right, currentMax);
  };
  helper(root, 0);
  return answer;
};

module.exports = countGoodNodes;
