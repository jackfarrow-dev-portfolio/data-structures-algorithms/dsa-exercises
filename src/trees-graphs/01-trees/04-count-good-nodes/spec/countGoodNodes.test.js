const expect = require('chai').expect;
const TreeNode = require('../../TreeNode');
const countGoodNodes = require('../countGoodNodes');

describe('countGoodNodes', () => {
  it('should return 4 when root = [3, 1, 4, 3, null, 1, 5]', () => {
    const one = new TreeNode(1);
    const five = new TreeNode(5);
    const three = new TreeNode(3);
    const one2 = new TreeNode(1, three);
    const four = new TreeNode(4, one, five);
    const root = new TreeNode(3, one2, four);

    expect(countGoodNodes(root)).to.equal(4);
  });

  it('should return 3 when root = [3, 3, null, 4, 2]', () => {
    const two = new TreeNode(2);
    const four = new TreeNode(4, two);
    const three = new TreeNode(3, null, four);
    const root = new TreeNode(3, three);

    expect(countGoodNodes(root)).to.equal(3);
  });

  it('should return 1 when root = [1]', () => {
    expect(countGoodNodes(new TreeNode(1))).to.equal(1);
  });
});
