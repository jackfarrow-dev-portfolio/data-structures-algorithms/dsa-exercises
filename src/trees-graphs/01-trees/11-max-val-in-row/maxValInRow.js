const TreeNode = require('../TreeNode');

/**
 *
 * @param {TreeNode} root
 * @returns {number[]}
 */
const maxValInRow = (root) => {
  if (!root) return [];
  const result = [];
  let queue = [root];

  while (queue.length) {
    let nodesInRow = queue.length;
    let nextQueue = [];
    let maxVal = -Infinity;

    for (let i = 0; i < nodesInRow; i++) {
      const node = queue[i];
      maxVal = Math.max(maxVal, node.val);
      if (node.left) nextQueue.push(node.left);
      if (node.right) nextQueue.push(node.right);
    }
    result.push(maxVal);
    queue = nextQueue;
  }

  return result;
};

module.exports = maxValInRow;
