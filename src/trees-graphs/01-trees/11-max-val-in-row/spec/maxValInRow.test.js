const expect = require('chai').expect;
const TreeNode = require('../../TreeNode');
const maxValInRow = require('../maxValInRow');

describe('maxValInRow', () => {
  it('should return [1, 3, 9] when root = [1, 3, 2, 5, 3, null, 9]', () => {
    const five = new TreeNode(5);
    const three = new TreeNode(3);
    const nine = new TreeNode(9);
    const three2 = new TreeNode(3, five, three);
    const two = new TreeNode(2, null, nine);
    const root = new TreeNode(1, three2, two);

    expect(maxValInRow(root)).to.be.an('array').that.includes(1);
    expect(maxValInRow(root)).to.be.an('array').that.includes(3);
    expect(maxValInRow(root)).to.be.an('array').that.includes(9);
  });

  it('should return [1, 3] when root = [1, 2, 3]', () => {
    const three = new TreeNode(3);
    const two = new TreeNode(2);
    const root = new TreeNode(1, two, three);

    expect(maxValInRow(root)).to.be.an('array').that.includes(1);
    expect(maxValInRow(root)).to.be.an('array').that.includes(3);
  });
});
