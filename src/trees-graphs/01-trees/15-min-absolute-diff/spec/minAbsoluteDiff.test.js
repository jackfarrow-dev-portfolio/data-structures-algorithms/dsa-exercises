const expect = require('chai').expect;
const TreeNode = require('../../TreeNode');
const minAbsoluteDiff = require('../minAbsoluteDiff');

describe('minAbsoluteDiff', () => {
  it('should return 1 when root = [4, 2, 6, 1, 3]', () => {
    const one = new TreeNode(1);
    const three = new TreeNode(3);
    const two = new TreeNode(2, one, three);
    const six = new TreeNode(6);
    const root = new TreeNode(4, two, six);

    expect(minAbsoluteDiff(root)).to.equal(1);
  });

  //   it('should return 1 when  root = [1, 0, 48, null, null, 12, 49]', () => {
  //     const twelve = new TreeNode(12);
  //     const fortyNine = new TreeNode(49);
  //     const zero = new TreeNode(0);
  //     const fortyEight = new TreeNode(48, twelve, fortyNine);
  //     const root = new TreeNode(1, zero, fortyEight);

  //     expect(minAbsoluteDiff(root)).to.equal(1);
  //   });
});
