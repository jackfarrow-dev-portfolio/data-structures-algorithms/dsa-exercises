const TreeNode = require('../TreeNode');

/**
 *
 * @param {TreeNode} root
 * @returns {number}
 */
const minAbsoluteDiff = (root) => {
  const values = [];

  const helper = (node) => {
    if (!node) return 0;
    helper(node.left);
    values.push(node.val);
    helper(node.right);
  };
  helper(root);
  let minAbsDiff = Math.abs(values[0] - values[1]);
  for (let i = 1; i < values.length - 1; i++) {
    minAbsDiff = Math.min(minAbsDiff, Math.abs(values[i] - values[i + 1]));
  }
  return minAbsDiff;
};

module.exports = minAbsoluteDiff;
