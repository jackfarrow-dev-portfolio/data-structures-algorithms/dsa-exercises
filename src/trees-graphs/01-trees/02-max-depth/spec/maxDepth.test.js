const expect = require('chai').expect;
const maxDepth = require('../maxDepth');
const TreeNode = require('../../TreeNode');

describe('maxDepth', () => {
  it('should return 3 when root = [3, 9, 20, null, null, 15, 7', () => {
    /*
                        3
                9               20
            n       n        15     7
        */
    const fifteen = new TreeNode(15);
    const seven = new TreeNode(7);
    const nine = new TreeNode(9);
    const twenty = new TreeNode(20, fifteen, seven);
    const root = new TreeNode(3, nine, twenty);
    expect(maxDepth(root)).to.equal(3);
  });

  it('should return 2 when root = [1, null, 2', () => {
    const two = new TreeNode(2);
    const root = new TreeNode(1, null, two);
    expect(maxDepth(root)).to.equal(2);
  });
});
