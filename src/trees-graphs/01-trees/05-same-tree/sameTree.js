const TreeNode = require('../TreeNode');

/**
 *
 * @param {TreeNode} p
 * @param {TreeNode} q
 * @returns {boolean}
 */
const isSameTree = (p, q) => {
  if (!p && !q) return true;

  if (!p || !q) return false;

  if (p.val !== q.val) return false;

  let left = isSameTree(p.left, q.left);
  let right = isSameTree(p.right, q.right);

  return left && right;
};

module.exports = isSameTree;
