const expect = require('chai').expect;
const TreeNode = require('../../TreeNode');
const isSameTree = require('../sameTree');

describe('isSameTree', () => {
  it('should return `true` when p = [1, 2, 3] and q = [1, 2, 3]', () => {
    const three = new TreeNode(3);
    const two = new TreeNode(2);
    const p = new TreeNode(1, two, three);
    const q = new TreeNode(1, two, three);

    expect(isSameTree(p, q)).to.be.true;
  });

  it('should return `false` when p = [1, 2] and q = [1, null, 2]', () => {
    const pTwo = new TreeNode(2);
    const pRoot = new TreeNode(1, pTwo);

    const qTwo = new TreeNode(2);
    const qRoot = new TreeNode(1, null, qTwo);

    expect(isSameTree(pRoot, qRoot)).to.be.false;
  });

  it('should return `false` when p = [1, 2, 1] and q = [1, 1, 2]', () => {
    const pOne = new TreeNode(1);
    const pTwo = new TreeNode(2);
    const pRoot = new TreeNode(1, pTwo, pOne);

    const qTwo = new TreeNode(2);
    const qOne = new TreeNode(1);
    const qRoot = new TreeNode(1, qOne, qTwo);

    expect(isSameTree(pRoot, qRoot)).to.be.false;
  });
});
