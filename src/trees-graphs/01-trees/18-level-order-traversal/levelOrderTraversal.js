/**
 *
 * @param {TreeNode} root
 * @returns {number[][]}
 */
const levelOrderTraversal = (root) => {
  if (!root) return [];
  const result = [];
  let queue = [root];

  while (queue.length) {
    let qLen = queue.length;
    let nextQueue = [];

    for (let i = 0; i < qLen; i++) {
      let node = queue[i];
      if (node.left) nextQueue.push(node.left);
      if (node.right) nextQueue.push(node.right);
    }
    result.push(queue.map((node) => node.val));
    queue = nextQueue;
  }
  return result;
};

module.exports = levelOrderTraversal;
