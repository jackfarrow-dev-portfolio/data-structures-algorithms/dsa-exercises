const expect = require('chai').expect;
const levelOrderTraversal = require('../levelOrderTraversal');
const TreeNode = require('../../TreeNode');

describe('levelOrderTraversal', () => {
  /*
Input: root = [3, 9, 20, null, null, 15, 7]
Output: [ [3], [9, 20], [15, 7]]
    */
  it('should return [ [3], [9, 20], [15, 7]] when root = [3, 9, 20, null, null, 15, 7]', () => {
    const seven = new TreeNode(7);
    const fifteen = new TreeNode(15);
    const nine = new TreeNode(9);
    const twenty = new TreeNode(20, fifteen, seven);
    const root = new TreeNode(3, nine, twenty);

    const result = levelOrderTraversal(root);
    expect(result[0][0]).to.equal(root.val);
    expect(result[1][0]).to.equal(nine.val);
    expect(result[1][1]).to.equal(twenty.val);
    expect(result[2][0]).to.equal(fifteen.val);
    expect(result[2][1]).to.equal(seven.val);
  });

  it('should return [[1]] when root = [1]', () => {
    expect(levelOrderTraversal(new TreeNode(1))[0][0]).to.equal(1);
  });

  it('should return [] when root = null', () => {
    expect(levelOrderTraversal(null).length).to.equal(0);
  });
});
