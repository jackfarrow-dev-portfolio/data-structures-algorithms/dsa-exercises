const expect = require('chai').expect;
const TreeNode = require('../../TreeNode');
const maxDiff = require('../maxDiff');

describe('maxDiff', () => {
  it('should return 7 when root = [8, 3, 10, 1, 6, null, 14, null, null, 4, 7, 13]', () => {
    const four = new TreeNode(4);
    const seven = new TreeNode(7);
    const thirteen = new TreeNode(13);
    const one = new TreeNode(1);
    const six = new TreeNode(6, four, seven);
    const fourteen = new TreeNode(14, thirteen);
    const three = new TreeNode(3, one, six);
    const ten = new TreeNode(10, null, fourteen);
    const eight = new TreeNode(8, three, ten);

    expect(maxDiff(eight)).to.equal(7);
  });

  it('should return 19 when root = [1, 10, 20, 9, 2, 7, 11]', () => {
    const nine = new TreeNode(9);
    const two = new TreeNode(2);
    const seven = new TreeNode(7);
    const eleven = new TreeNode(11);
    const ten = new TreeNode(10, nine, two);
    const twenty = new TreeNode(20, seven, eleven);
    const root = new TreeNode(1, ten, twenty);

    expect(maxDiff(root)).to.equal(19);
  });

  it('should return 3 when root = [1, null, 2, null, 0, 3]', () => {
    const three = new TreeNode(3);
    const zero = new TreeNode(0, three);
    const two = new TreeNode(2, null, zero);
    const root = new TreeNode(1, null, two);

    expect(maxDiff(root)).to.equal(3);
  });
});
