# Maximum Difference Between Node and Ancestor

Given the `root` of a binary tree, find the maximum value `v` for which there
exist DIFFERENT nodes `a` and `b` where v = |a.val - b.val| and `a` is an
ancestor of `b`.

A node `a` is an ancestor of `b` if either: - Any child of `a` is equal to `b`
OR - Any child of `a` is an ancestor of `b`

## Example 1:

                                8
                    3                       10
                1        6                      14
                        4  7                   13



- Input: root = [8, 3, 10, 1, 6, null, 14, null, null, 4, 7, 13]
- Output: 7
- Explanation:

We have various ancestor-node differences, some of which are given below: |8 -
3| = 5 |3 - 7| = 4 |8 - 1| = 7 |10 -13| = 3

Among all differences, the maximum value of 7 is obtained by |8 - 1| = 7.
