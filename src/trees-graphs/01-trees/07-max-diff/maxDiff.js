const TreeNode = require('../TreeNode');

/**
 *
 * @param {TreeNode} root
 */
const maxDiff = (root) => {
  if (!root) return null;
  let maxDiff = -Infinity;

  const helper = (node, maxAncestor, minVal) => {
    if (!node) return 0;

    let currentResult = Math.max(
      Math.abs(maxAncestor - node.val),
      Math.abs(minVal - node.val)
    );

    maxDiff = Math.max(maxDiff, currentResult);

    maxAncestor = Math.max(maxAncestor, node.val);
    minVal = Math.min(minVal, node.val);

    helper(node.left, maxAncestor, minVal);
    helper(node.right, maxAncestor, minVal);

    return;
  };
  helper(root, root.val, root.val);
  return maxDiff;
};

module.exports = maxDiff;
