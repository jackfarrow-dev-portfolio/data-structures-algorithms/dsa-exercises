const TreeNode = require('../TreeNode');

/**
 *
 * @param {TreeNode} root
 * @param {number} target
 * @returns {boolean}
 */
const pathSum = (root, target) => {
  let answer;

  const helper = (node, currentSum) => {
    if (!node) return false;
    currentSum += node.val;

    if (!node.left && !node.right) {
      if (currentSum === target) return true;
    }
    let left = helper(node.left, currentSum);
    let right = helper(node.right, currentSum);
    return left || right;
  };
  answer = helper(root, 0);
  return answer;
};

module.exports = pathSum;
