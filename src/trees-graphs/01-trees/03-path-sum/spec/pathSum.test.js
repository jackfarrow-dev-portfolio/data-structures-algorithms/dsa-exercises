const expect = require('chai').expect;
const pathSum = require('../pathSum');
const TreeNode = require('../../TreeNode');

describe('pathSum', () => {
  it('should return true if root = [[5, 4, 8, 11, null, 13, 4, 7, 2, null, null, null, 1] and targetSum = 22', () => {
    const seven = new TreeNode(7);
    const two = new TreeNode(2);
    const one = new TreeNode(1);
    const eleven = new TreeNode(11, seven, two);
    const thirteen = new TreeNode(13);
    const four = new TreeNode(4, null, one);
    const four2 = new TreeNode(4, eleven);
    const eight = new TreeNode(8, thirteen, four);
    const root = new TreeNode(5, four2, eight);

    expect(pathSum(root, 22)).to.be.true;
  });

  it('should return false if root = [1, 2, 3] and target = 5', () => {
    const three = new TreeNode(3);
    const two = new TreeNode(2);
    const root = new TreeNode(1, two, three);

    expect(pathSum(root, 5)).to.be.false;
  });

  it('should return false if root is null', () => {
    expect(pathSum(null, 0)).to.be.false;
  });
});
