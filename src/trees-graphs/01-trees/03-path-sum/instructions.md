# Path Sum

Given the `root` of a binary tree and an integer `targetSum`, return `true` if
there exists a path from the root to a leaf such that the sum of the nodes on
the path is equal to `targetSum` or return `false` otherwise.

## Example 1:

```

                        5
                4               8
            11              13      4
        7       2                       1
```

- Input: root = [5, 4, 8, 11, null, 13, 4, 7, 2, null, null, null, 1], targetSum
  = 22
- Output: true
- Explanation: The path [5, 4, 11, 2] sums to 22.

## Example 2:

- Input: root = [1,2,3], targetSum = 5
- Output: false
- Explanation: There two root-to-leaf paths in the tree: (1 --> 2): The sum
  is 3. (1 --> 3): The sum is 4. There is no root-to-leaf path with sum = 5.

## Example 3:

- Input: root = [], targetSum = 0
- Output: false
- Explanation: Since the tree is empty, there are no root-to-leaf paths.
