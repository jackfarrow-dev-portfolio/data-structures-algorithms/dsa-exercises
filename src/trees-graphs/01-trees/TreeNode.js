class TreeNode {
  constructor(val, left, right) {
    this.val = val !== undefined ? val : 0;
    this.left = left !== undefined ? left : null;
    this.right = right !== undefined ? right : null;
  }
}

module.exports = TreeNode;
