const expect = require('chai').expect;
const TreeNode = require('../../TreeNode');
const rangeSumBst = require('../rangeSumBst');

describe('rangeSumBst', () => {
  it('should return 32 when root = [10, 5, 15, 3, 7, null, 18], low = 7, high = 15', () => {
    const three = new TreeNode(3);
    const seven = new TreeNode(7);
    const eighteen = new TreeNode(18);
    const five = new TreeNode(5, three, seven);
    const fifteen = new TreeNode(15, null, eighteen);
    const root = new TreeNode(10, five, fifteen);

    expect(rangeSumBst(root, 7, 15)).to.equal(32);
  });

  it('should return 23 when root = [10, 5, 15, 3, 7, 13, 18, 1, null, 6], low = 6, high = 10', () => {
    const one = new TreeNode(1);
    const six = new TreeNode(6);
    const three = new TreeNode(3, one);
    const seven = new TreeNode(7, six);
    const thirteen = new TreeNode(13);
    const eighteen = new TreeNode(18);
    const five = new TreeNode(5, three, seven);
    const fifteen = new TreeNode(15, thirteen, eighteen);
    const root = new TreeNode(10, five, fifteen);

    expect(rangeSumBst(root, 6, 10)).to.equal(23);
  });
});
