const TreeNode = require('../TreeNode');

/**
 *
 * @param {TreeNode} root
 * @param {number} low
 * @param {number} high
 */
const rangeSumBst = (root, low, high) => {
  if (!root) return 0;
  let answer = 0;

  const helper = (node) => {
    if (!node) return;

    if (node.val >= low && node.val <= high) {
      answer += node.val;
    }
    if (node.val >= low) {
      helper(node.left);
    }
    if (node.val <= high) {
      helper(node.right);
    }
  };
  helper(root);

  return answer;
};

module.exports = rangeSumBst;
