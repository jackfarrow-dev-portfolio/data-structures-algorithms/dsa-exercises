const TreeNode = require('../TreeNode');

/**
 *
 * @param {TreeNode} root
 */
const minDepth = (root) => {
  if (!root) return 0;
  let answer = Infinity;

  const helper = (node, currentDepth) => {
    if (!node) return 0;

    if (!node.left && !node.right) {
      answer = Math.min(answer, currentDepth);
    }

    helper(node.left, currentDepth + 1);
    helper(node.right, currentDepth + 1);
    return;
  };
  helper(root, 1);
  return answer;
};

module.exports = minDepth;
