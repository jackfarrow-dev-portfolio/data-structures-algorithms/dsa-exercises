const expect = require('chai').expect;
const TreeNode = require('../../TreeNode');
const minDepth = require('../minDepth');

describe('minDepth', () => {
  it('should return 2 when root = [3, 9, 20, null, null, 15, 7]', () => {
    const fifteen = new TreeNode(15);
    const seven = new TreeNode(7);
    const nine = new TreeNode(9);
    const twenty = new TreeNode(20, fifteen, seven);
    const root = new TreeNode(3, nine, twenty);
    expect(minDepth(root)).to.equal(2);
  });

  it('should return 5 when root = [2, null, 3, null, 4, null, 5, null, 6]', () => {
    const six = new TreeNode(6);
    const five = new TreeNode(5, null, six);
    const four = new TreeNode(4, null, five);
    const three = new TreeNode(3, null, four);
    const root = new TreeNode(2, null, three);
    expect(minDepth(root)).to.equal(5);
  });
});
