const expect = require('chai').expect;
const spellsAndPotions = require('../spellsAndPotions');

describe('spellsAndPotions', () => {
  it('should return [4, 0, 3] when spells = [5, 1, 3] and potions = [1, 2, 3, 4, 5] and success = 7', () => {
    const spells = [5, 1, 3];
    const potions = [1, 2, 3, 4, 5];
    const success = 7;

    expect(spellsAndPotions(spells, potions, success)).to.deep.equal([4, 0, 3]);
  });

  it('should return [2, 0, 2] when spells = [3, 1, 2] and potions = [8, 5, 8] and success = 16', () => {
    const spells = [3, 1, 2];
    const potions = [8, 5, 8];
    const success = 16;
    expect(spellsAndPotions(spells, potions, success)).to.deep.equal([2, 0, 2]);
  });
});
