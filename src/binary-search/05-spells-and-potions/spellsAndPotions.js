/**
 *
 * @param {number[]} spells
 * @param {number[]} potions
 * @param {number} success
 * @returns
 */
const spellsAndPotions = (spells, potions, success) => {
  const result = [];
  const m = spells.length;
  const n = potions.length;
  potions = potions.sort((a, b) => a - b);

  const binarySearch = (spell) => {
    let left = 0;
    let right = n - 1;
    let middle;
    let minSuccessfulCombination = Math.ceil(success / spell);

    while (left <= right) {
      middle = Math.floor((right + left) / 2);
      if (potions[middle] < minSuccessfulCombination) {
        left = middle + 1;
      } else if (potions[middle] > minSuccessfulCombination) {
        right = middle - 1;
      } else {
        return n - middle;
      }
    }
    return n - left;
  };

  for (let i = 0; i < m; i++) {
    result.push(binarySearch(spells[i]));
  }

  return result;
};

module.exports = spellsAndPotions;
