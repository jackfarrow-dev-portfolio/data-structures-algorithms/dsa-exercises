const expect = require('chai').expect;
const findInsertPosition = require('../findInsertPosition');

describe('findInsertPosition', () => {
  it('should return 2 when nums = [1, 3, 5, 6] and target = 5', () => {
    expect(findInsertPosition([1, 3, 5, 6], 5)).to.equal(2);
  });

  it('should return 1 when nums = [1, 3, 5, 6] and target = 2', () => {
    expect(findInsertPosition([1, 3, 5, 6], 2)).to.equal(1);
  });

  it('should return 4 when nums = [1, 3, 5, 6] and target = 7', () => {
    expect(findInsertPosition([1, 3, 5, 6], 7)).to.equal(4);
  });
});
