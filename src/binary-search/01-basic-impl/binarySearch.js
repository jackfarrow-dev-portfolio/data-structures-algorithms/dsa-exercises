const binarySearch = (arr, target) => {
  let left = 0;
  let right = arr.length - 1;

  while (left <= right) {
    let middle = Math.floor((right + left) / 2);
    if (arr[middle] === target) return middle;
    else if (arr[middle] > target) right = middle - 1;
    else left = middle + 1;
  }

  // Target is not in arr, but left is insertion point
  return left;
};

module.exports = binarySearch;
