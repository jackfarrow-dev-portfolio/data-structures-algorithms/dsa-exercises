const expect = require('chai').expect;
const binarySearch = require('../binarySearch');

describe('binarySearch implementation', () => {
  it('should return the index of the target if found', () => {
    const arr = [1, 2, 3, 4];
    const target = 3;
    expect(binarySearch(arr, target)).to.equal(2);
  });

  it('should return 0 if target < arr[0]', () => {
    const arr = [1, 2, 3, 4];
    const target = 0;
    expect(binarySearch(arr, target)).to.equal(0);
  });

  it('should return arr.length if target > arr[arr.length - 1]', () => {
    const arr = [1, 2, 3, 4];
    const target = 7;
    expect(binarySearch(arr, target)).to.equal(arr.length);
  });

  it('should return the correct insertion index if target not found && target > arr[0] && target < arr[arr.length - 1]', () => {
    const arr = [1, 2, 4, 5];
    const target = 3;
    expect(binarySearch(arr, target)).to.equal(2);
  });
});
