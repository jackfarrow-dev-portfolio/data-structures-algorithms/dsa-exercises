const expect = require('chai').expect;
const binarySearch = require('../binarySearch');

describe('binarySearch', () => {
  it('should return i if nums[i] == target', () => {
    const nums = [-1, 0, 3, 5, 9, 12];
    const target = 9;
    expect(binarySearch(nums, target)).to.equal(4);
  });

  it('should return -1 if target not in nums', () => {
    const nums = [-1, 0, 3, 5, 9, 12];
    const target = 2;
    expect(binarySearch(nums, target)).to.equal(-1);
  });
});
