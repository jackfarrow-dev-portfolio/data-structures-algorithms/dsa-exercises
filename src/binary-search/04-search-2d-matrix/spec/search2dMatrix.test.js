const expect = require('chai').expect;
const search2dMatrix = require('../search2dMatrix');

describe('search2dMatrix', () => {
  it('should return true if target is in matrix', () => {
    const matrix = [
      [1, 3, 5, 7],
      [10, 11, 16, 20],
      [23, 30, 34, 60],
    ];
    const target = 3;
    expect(search2dMatrix(matrix, target)).to.equal(true);
  });

  it('should return false if target not in matrix', () => {
    const matrix = [
      [1, 3, 5, 7],
      [10, 11, 16, 20],
      [23, 30, 34, 60],
    ];
    const target = 13;
    expect(search2dMatrix(matrix, target)).to.equal(false);
  });
});
