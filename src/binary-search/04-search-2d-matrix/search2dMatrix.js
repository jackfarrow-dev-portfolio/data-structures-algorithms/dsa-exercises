/**
 *
 * @param {number[][]} matrix
 * @param {number} target
 */
const search2dMatrix = (matrix, target) => {
  const m = matrix.length;
  const n = matrix[0].length;

  const binarySearch = (arr) => {
    let left = 0;
    let right = n - 1;
    while (left <= right) {
      let middle = Math.floor((left + right) / 2);
      if (arr[middle] === target) {
        return true;
      } else if (arr[middle] > target) {
        right = middle - 1;
      } else {
        left = middle + 1;
      }
    }
  };

  for (let i = 0; i < m; i++) {
    let answer = binarySearch(matrix[i]);
    if (answer) return answer;
  }
  return false;
};
// const search2dMatrix = (matrix, target) => {
//   const m = matrix.length;
//   const n = matrix[0].length;
//   let answer = false;

//   const binarySearch = (nums, target) => {
//     let left = 0;
//     let right = n - 1;
//     while (left <= right) {
//       let middle = Math.floor((right + left) / 2);
//       if (nums[middle] === target) {
//         answer = true;
//         return;
//       } else if (nums[middle] > target) right = middle - 1;
//       else left = middle + 1;
//     }
//   };

//   for (let i = 0; i < m; i++) {
//     let x = matrix[i];
//     if (target >= x[0] && target <= x[n - 1]) {
//       binarySearch(matrix[i], target);
//     }
//   }
//   return answer;
// };

module.exports = search2dMatrix;
