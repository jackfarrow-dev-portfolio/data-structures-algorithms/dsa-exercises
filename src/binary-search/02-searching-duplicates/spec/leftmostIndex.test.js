const expect = require('chai').expect;
const findLeftmostIndex = require('../leftmostIndex');

describe('findLeftmostIndex', () => {
  it('should return the leftmost index of target if duplicate targets in arr', () => {
    const arr = [1, 2, 2, 3, 4];
    const target = 2;
    expect(findLeftmostIndex(arr, target)).to.equal(1);
  });

  it('should return i if arr[i] === target', () => {
    const arr = [1, 2, 3, 4];
    const target = 4;
    expect(findLeftmostIndex(arr, target)).to.equal(3);
  });

  it('should return 0 if target < arr[0]', () => {
    const arr = [1, 2, 3, 4];
    const target = 0;
    expect(findLeftmostIndex(arr, target)).to.equal(0);
  });
});
