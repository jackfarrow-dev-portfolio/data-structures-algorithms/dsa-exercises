const findLeftmostIndex = (arr, target) => {
  let left = 0;
  let right = arr.length - 1;

  while (left < right) {
    let middle = Math.floor((right + left) / 2);
    if (arr[middle] >= target) {
      right = middle;
    } else {
      left = middle + 1;
    }
  }

  return left;
};

module.exports = findLeftmostIndex;
