const expect = require('chai').expect;
const firstCharTwice = require('../firstCharTwice');

describe('firstCharTwice', () => {
  it('should return c if `s` = "abccbaacz"', () => {
    const s = 'abccbaacz';
    expect(firstCharTwice(s)).to.equal('c');
  });

  it('should return "d" when `s` = "abcdd', () => {
    expect(firstCharTwice('abcdd')).to.equal('d');
  });
});
