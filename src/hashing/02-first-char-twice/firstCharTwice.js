/**
 *
 * @param {string} s
 * @returns {string}
 */
const firstCharTwice = (s) => {
  const map = new Map();

  for (let i = 0; i < s.length; i++) {
    if (!map.has(s[i])) {
      map.set(s[i], 1);
    } else {
      return s[i];
    }
  }
};

module.exports = firstCharTwice;
