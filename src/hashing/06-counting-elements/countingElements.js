/**
 *
 * @param {number[]} arr
 * @returns {number}
 */
const countElements = (arr) => {
  const map = new Map();

  for (let i = 0; i < arr.length; i++) {
    if (!map.has(arr[i])) {
      map.set(arr[i], 1);
    } else {
      let count = map.get(arr[i]);
      map.set(arr[i], count + 1);
    }
  }
  let answer = 0;

  map.forEach((val, key) => {
    if (map.has(key + 1)) answer += val;
  });
  return answer;
};

module.exports = countElements;
