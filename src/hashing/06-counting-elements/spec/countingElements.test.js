const expect = require('chai').expect;
const countElements = require('../countingElements');

describe('countElements', () => {
  it('should return 2 when arr = [1, 2, 3]', () => {
    expect(countElements([1, 2, 3])).to.equal(2);
  });

  it('should return 0 when arr = [1, 1, 3, 3, 5, 5, 7, 7]', () => {
    expect(countElements([1, 1, 3, 3, 5, 5, 7, 7])).to.equal(0);
  });
});
