const expect = require('chai').expect;
const findPlayers = require('../findPlayers');

describe('findPlayers', () => {
  it('should return expected results', () => {
    const matches = [
      [1, 3],
      [2, 3],
      [3, 6],
      [5, 6],
      [5, 7],
      [4, 5],
      [4, 8],
      [4, 9],
      [10, 4],
      [10, 9],
    ];

    const output = [
      [1, 2, 10],
      [4, 5, 7, 8],
    ];

    expect(findPlayers(matches)).to.deep.equal(output);
  });

  it('should return expected results when', () => {
    const matches = [
      [2, 3],
      [1, 3],
      [5, 4],
      [6, 4],
    ];

    const output = [[1, 2, 5, 6], []];

    expect(findPlayers(matches)).to.deep.equal(output);
  });
});
