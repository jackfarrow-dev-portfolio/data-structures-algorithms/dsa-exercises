/**
 *
 * @param {number[][]} matches
 * @returns {number[][]}
 */
const findPlayers = (matches) => {
  const winners = new Set();
  const losers = new Map();

  for (const m of matches) {
    const [w, l] = m;
    winners.add(w);
    if (!losers.has(l)) {
      losers.set(l, 1);
    } else {
      const count = losers.get(l);
      losers.set(l, count + 1);
    }
  }

  const undefeated = [];
  const oneLoss = [];

  losers.forEach((val, key) => {
    if (val === 1) oneLoss.push(key);
  });

  winners.forEach((val, _) => {
    if (!losers.has(val)) {
      undefeated.push(val);
    }
  });

  return [undefeated.sort((a, b) => a - b), oneLoss.sort((a, b) => a - b)];
};

module.exports = findPlayers;
