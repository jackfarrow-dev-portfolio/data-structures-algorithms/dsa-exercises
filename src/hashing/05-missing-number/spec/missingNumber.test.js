const expect = require('chai').expect;
const missingNumbers = require('../missingNumber');

describe('missingNumbers', () => {
  it('should return 2 when nums = [3, 0, 1]', () => {
    expect(missingNumbers([3, 0, 1])).to.equal(2);
  });

  it('should return 2 when nums = [0, 1]', () => {
    expect(missingNumbers([0, 1])).to.equal(2);
  });

  it('should return 8 when nums = [9, 6, 4, 2, 3, 5, 7, 0, 1]', () => {
    expect(missingNumbers([9, 6, 4, 2, 3, 5, 7, 0, 1])).to.equal(8);
  });
});
