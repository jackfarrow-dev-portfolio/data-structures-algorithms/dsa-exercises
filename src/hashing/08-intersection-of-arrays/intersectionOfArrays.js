/**
 *
 * @param {number[][]} nums
 * @returns {number[]}
 */

const intersectionOfArrays = (nums) => {
  const result = [];
  const n = nums.length;
  const map = new Map();

  for (let i = 0; i < n; i++) {
    for (let j = 0; j < nums[i].length; j++) {
      if (!map.has(nums[i][j])) {
        map.set(nums[i][j], 1);
      } else {
        const count = map.get(nums[i][j]);
        map.set(nums[i][j], count + 1);
      }
    }
  }

  map.forEach((val, key) => {
    if (val === n) result.push(key);
  });
  return result.sort((a, b) => a - b);
};

module.exports = intersectionOfArrays;
