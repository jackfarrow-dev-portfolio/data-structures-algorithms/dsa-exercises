const expect = require('chai').expect;
const intersectionOfArrays = require('../intersectionOfArrays');

describe('intersectionOfArrays', () => {
  it('should return an array containing [3, 4] when nums = [[3,1,2,4,5],[1,2,3,4],[3,4,5,6]]', () => {
    expect(
      intersectionOfArrays([
        [3, 1, 2, 4, 5],
        [1, 2, 3, 4],
        [3, 4, 5, 6],
      ])
    ).to.deep.equal([3, 4]);
  });

  it('should return an array containing [] when nums = [[1,2,3],[4,5,6]]', () => {
    expect(
      intersectionOfArrays([
        [1, 2, 3],
        [4, 5, 6],
      ])
    ).to.deep.equal([]);
  });
});
