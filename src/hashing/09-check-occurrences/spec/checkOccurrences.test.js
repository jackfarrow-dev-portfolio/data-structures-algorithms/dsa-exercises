const expect = require('chai').expect;
const checkOccurrences = require('../checkOccurrences');

describe('checkOccurrences', () => {
  it('should return true if s = "abacbc"', () => {
    expect(checkOccurrences('abacbc')).to.equal(true);
  });

  it('should return false if s = "aaabb"', () => {
    expect(checkOccurrences('aaabb')).to.equal(false);
  });
});
