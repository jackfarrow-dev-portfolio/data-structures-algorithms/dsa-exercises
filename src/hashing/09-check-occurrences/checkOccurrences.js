/**
 *
 * @param {string} s
 * @returns {boolean}
 */

const checkOccurrences = (s) => {
  const map = new Map();

  for (let i = 0; i < s.length; i++) {
    if (!map.has(s[i])) {
      map.set(s[i], 1);
    } else {
      const count = map.get(s[i]);
      map.set(s[i], count + 1);
    }
  }

  const target = map.get(s[0]);

  let result = true;

  map.forEach((val, _) => {
    if (val !== target) {
      result = false;
      return;
    }
  });

  return result;
};

module.exports = checkOccurrences;
