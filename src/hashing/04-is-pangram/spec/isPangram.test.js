const expect = require('chai').expect;
const isPangram = require('../isPangram');

describe('isPangram', () => {
  it('should return true when `s` = "thequickbrownfoxjumpsoverthelazydog"', () => {
    expect(isPangram('thequickbrownfoxjumpsoverthelazydog')).to.equal(true);
  });

  it('should return false when `s` = "leetcode"', () => {
    expect(isPangram('leetcode')).to.equal(false);
  });
});
