/**
 *
 * @param {string} s
 * @returns {boolean}
 */
const isPangram = (s) => {
  const alphabet = [
    'a',
    'b',
    'c',
    'd',
    'e',
    'f',
    'g',
    'h',
    'i',
    'j',
    'k',
    'l',
    'm',
    'n',
    'o',
    'p',
    'q',
    'r',
    's',
    't',
    'u',
    'v',
    'w',
    'x',
    'y',
    'z',
  ];

  const strAsSet = new Set();

  const set = new Set(alphabet);
  for (let i = 0; i < s.length; i++) {
    strAsSet.add(s[i]);
  }

  return set.size == strAsSet.size;
};

module.exports = isPangram;
