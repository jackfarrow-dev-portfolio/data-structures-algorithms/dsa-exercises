const expect = require('chai').expect;
const twoSum = require('../twoSum');

describe('twoSum', () => {
  it('should return [0, 1] whent nums = [2,7,11,15] and target = 9', () => {
    const nums = [2, 7, 11, 15];
    const target = 9;
    expect(twoSum(nums, target)).to.include(0);
    expect(twoSum(nums, target)).to.include(1);
  });

  it('should return [1, 2] when nums = [3, 2, 4] and target = 6', () => {
    const nums = [3, 2, 4];
    const target = 6;
    expect(twoSum(nums, target)).to.include(2);
    expect(twoSum(nums, target)).to.include(1);
  });

  it('should return [0, 1] when nums = [3, 3] and target = 6', () => {
    const nums = [3, 3];
    const target = 6;
    expect(twoSum(nums, target)).to.include(0);
    expect(twoSum(nums, target)).to.include(1);
  });

  it('should return [] when nums = [0, 4, 3, 0] and target = 0', () => {
    const nums = [0, 4, 3, 0];
    const target = 0;
    expect(twoSum(nums, target)).to.include(0);
    expect(twoSum(nums, target)).to.include(3);
  });
});
