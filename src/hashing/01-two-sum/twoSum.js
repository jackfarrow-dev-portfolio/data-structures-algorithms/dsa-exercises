/**
 *
 * @param {number[]} nums
 * @param {number} target
 * @returns {number}
 */

// LEETCODE SOLUTION
// const twoSum = (nums, target) => {
//   const map = new Map();

//   for (let i = 0; i < nums.length; i++) {
//     let num = nums[i];
//     let complement = target - num;
//     if (map.has(complement)) {
//       return [i, map.get(complement)];
//     }
//     map.set(num, i);
//   }
// };

// MY Solution; Works but is naive
const twoSum = (nums, target) => {
  const answer = [];
  const n = nums.length;
  const map = new Map();

  for (let i = 0; i < n; i++) {
    map.set(i, null);
    for (let j = 0; j < n; j++) {
      let sum = nums[i] + nums[j];
      if (j !== i && sum === target) {
        map.set(i, sum);
      }
    }
  }
  map.forEach((val, key) => {
    if (val === target) answer.push(key);
  });
  return answer;
};

// const twoSum = (nums, target) => {
//   const answer = [];
//   for (let i = 0; i < nums.length; i++) {
//     for (let j = 0; j < nums.length; j++) {
//       if (nums[i] + nums[j] === target && i !== j) {
//         answer.push(i);
//         answer.push(j);
//         return answer;
//       }
//     }
//   }
//   return answer;
// };

twoSum([3, 2, 4], 6);
module.exports = twoSum;
