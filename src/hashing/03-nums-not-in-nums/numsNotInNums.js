/**
 *
 * @param {number[]} nums
 * @returns {number[]}
 */
const numsNotInNums = (nums) => {
  const answer = [];
  const set = new Set(nums);

  set.forEach((val) => {
    if (!set.has(val - 1) && !set.has(val + 1)) {
      answer.push(val);
    }
  });
  return answer;
};

module.exports = numsNotInNums;
