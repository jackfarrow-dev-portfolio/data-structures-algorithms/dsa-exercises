# Numbers Not In `nums`

> Given an integer array `nums` find all the numbers `x` in `nums` that satisfy
> the following: `x + 1` is not in `nums`, and `x - 1` is not in nums.
