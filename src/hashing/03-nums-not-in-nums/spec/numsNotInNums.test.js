const expect = require('chai').expect;
const numsNotInNums = require('../numsNotInNums');

describe('numsNotInNums', () => {
  it('should return [3] when nums = [0, 1, 3, 5, 6]', () => {
    expect(numsNotInNums([0, 1, 3, 5, 6])).to.deep.equal([3]);
  });
});
