const expect = require('chai').expect;
const kDistinctChars = require('../kDistinctChars');

describe('kDistinctChars', () => {
  it('should return 3 if s = "eceba" and k = 2', () => {
    expect(kDistinctChars('eceba', 2)).to.equal(3);
  });

  it('should return 3 if s = "bourbon" and k = 3', () => {
    expect(kDistinctChars('bourbon', 3));
  });
});
