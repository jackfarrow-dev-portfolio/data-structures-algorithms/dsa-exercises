# Count Distinct Characters

> You are given a string `s` and an integer `k`. Find the length of the longest
> substring that contains **at most** `k` distinct characters.

## Example 1

- Input: `s = 'eceba'`, `k = 2`
- Output: 3
- Explanation: The longest substring with at most 2 distinct characters is`ece`.

## Example 2

- Input: `s = 'bourbon', k = 3`
- Output: 3
- Explanation: There are two substrings with at most 3 unique characters: `bou`
  and `rbo`. Both have exactly 3 unique characters.
