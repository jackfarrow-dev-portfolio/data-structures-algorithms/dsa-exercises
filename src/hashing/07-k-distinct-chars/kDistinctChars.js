/**
 *
 * @param {string} s
 * @returns {number}
 */
const kDistinctChars = (s, k) => {
  const map = new Map();

  let left = 0;
  let right;
  let ans = 0;
  let currentDistinctCount = 0;

  for (right = 0; right < s.length; right++) {
    if (!map.has(s[right])) {
      map.set(s[right], 1);
      currentDistinctCount++;
    }

    while (currentDistinctCount > k) {
      left++;
      currentDistinctCount--;
    }
    ans = Math.max(ans, right - left + 1);
  }

  return ans;
};

module.exports = kDistinctChars;
