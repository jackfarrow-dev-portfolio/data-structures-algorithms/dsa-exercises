const expect = require('chai').expect;
const moveZeroes = require('../moveZeroes');

describe('moveZeroes', () => {
  it('should return [1,3,12,0,0] when nums = [0, 1, 0, 3, 12]', () => {
    const nums = [1, 3, 12, 0, 0];
    moveZeroes(nums);
    expect(nums[0]).to.equal(1);
    expect(nums[1]).to.equal(3);
    expect(nums[2]).to.equal(12);
    expect(nums[3]).to.equal(0);
    expect(nums[4]).to.equal(0);
  });

  it('should return [0] when nums = [0]', () => {
    const nums = [0];
    moveZeroes(nums);
    expect(nums[0]).to.equal(0);
  });

  it('should return [1, 0, 0] when nums = [0, 0, 1]', () => {
    const nums = [0, 0, 1];
    moveZeroes(nums);
    expect(nums[0]).to.equal(1);
    expect(nums[1]).to.equal(0);
    expect(nums[2]).to.equal(0);
  });
});
