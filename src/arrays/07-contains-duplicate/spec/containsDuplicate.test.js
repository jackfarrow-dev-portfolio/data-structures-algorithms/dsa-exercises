const expect = require('chai').expect;
const containsDuplicate = require('../containsDuplicate');

describe('containsDuplicate', () => {
  it('should return true if nums = [1, 2, 3, 1]', () => {
    expect(containsDuplicate([1, 2, 3, 1])).to.be.true;
  });

  it('should return false if nums = [1, 2, 3, 4]', () => {
    expect(containsDuplicate([1, 2, 3, 4])).to.be.false;
  });

  it('should return true if nums = [1,1,1,3,3,4,3,2,4,2]', () => {
    expect(containsDuplicate([1, 1, 1, 3, 3, 4, 3, 2, 4, 2])).to.be.true;
  });
});
