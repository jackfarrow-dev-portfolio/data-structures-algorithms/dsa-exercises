/**
 *
 * @param {number[]} nums
 * @returns {boolean}
 */
const containsDuplicate = (nums) => {
  const map = new Map();
  for (let i = 0; i < nums.length; i++) {
    if (!map.has(nums[i])) {
      map.set(nums[i], true);
    } else {
      return true;
    }
  }
  return false;
};

module.exports = containsDuplicate;
