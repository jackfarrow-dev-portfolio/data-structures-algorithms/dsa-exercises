const expect = require('chai').expect;
const singleNumber = require('../singleNumber');

describe('singleNumber', () => {
  it('should return 1 if nums = [2, 2, 1]', () => {
    expect(singleNumber([2, 2, 1])).to.equal(1);
  });

  it('should return 4 if nums = [4, 1, 2, 1, 2]', () => {
    expect(singleNumber([4, 1, 2, 1, 2])).to.equal(4);
  });

  it('should return 1 if nums = [1]', () => {
    expect(singleNumber([1])).to.equal(1);
  });
});
