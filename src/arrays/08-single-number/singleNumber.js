/**
 *
 * @param {number[]} nums
 * @returns {number}
 */
const singleNumber = (nums) => {
  const map = new Map();
  let answer;
  for (let i = 0; i < nums.length; i++) {
    if (!map.has(nums[i])) {
      map.set(nums[i], 1);
    } else {
      let count = map.get(nums[i]);
      map.set(nums[i], count + 1);
    }
  }
  map.forEach((val, key) => {
    if (val === 1) answer = key;
  });
  return answer;
};

module.exports = singleNumber;
