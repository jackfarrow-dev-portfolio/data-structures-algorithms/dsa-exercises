const expect = require('chai').expect;
const bestTime = require('../bestTime');

describe('bestTimeToBuy', () => {
  it('should return 7 when prices = [7,1,5,3,6,4]', () => {
    expect(bestTime([7, 1, 5, 3, 6, 4])).to.equal(7);
  });

  it('should return 4 when prices = [1,2,3,4,5]', () => {
    expect(bestTime([1, 2, 3, 4, 5])).to.equal(4);
  });

  it('should equal 0 when prices = [7, 6, 4, 3, 1]', () => {
    expect(bestTime([7, 6, 4, 3, 1])).to.equal(0);
  });
});
