/**
 *
 * @param {number[]} prices
 * @returns {number}
 */
const bestTime = (prices) => {
  let answer = 0;
  for (let i = 1; i < prices.length; i++) {
    let profitOrLoss = prices[i] - prices[i - 1];
    if (profitOrLoss > 0) {
      answer += profitOrLoss;
    }
  }

  return answer > 0 ? answer : 0;
};

module.exports = bestTime;
