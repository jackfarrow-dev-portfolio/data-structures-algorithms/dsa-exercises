const expect = require('chai').expect;
const firstDuplicate = require('../firstDuplicate');

describe('firstDuplicate', () => {
  it('should return 3 if a = [2, 1, 3, 5, 3, 2]', () => {
    expect(firstDuplicate([2, 1, 3, 5, 3, 2])).to.equal(3);
  });

  it('should return 2 if a = [2, 2]', () => {
    expect(firstDuplicate([2, 2])).to.equal(2);
  });

  it('should return -1 if input = [2, 4, 3, 5, 1]', () => {
    expect(firstDuplicate([2, 4, 3, 5, 1])).to.equal(-1);
  });
});
