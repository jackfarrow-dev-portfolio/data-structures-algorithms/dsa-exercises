/**
 *
 * @param {number[]} a
 */
const firstDuplicate = (a) => {
  const map = new Map();
  for (let i = 0; i < a.length; i++) {
    if (!map.has(a[i])) {
      map.set(a[i], 1);
    } else {
      return a[i];
    }
  }
  return -1;
};

module.exports = firstDuplicate;
