/**
 *
 * @param {string} s
 * @returns {string}
 */
const firstNonRepeat = (s) => {
  const map = new Map();
  for (let i = 0; i < s.length; i++) {
    if (!map.has(s[i])) {
      map.set(s[i], 1);
    } else {
      const count = map.get(s[i]);
      map.set(s[i], count + 1);
    }
  }

  let nonDupes = [];

  map.forEach((val, key) => {
    if (val === 1) nonDupes.push(key);
  });

  return nonDupes.length ? nonDupes[0] : '_';
};

module.exports = firstNonRepeat;
