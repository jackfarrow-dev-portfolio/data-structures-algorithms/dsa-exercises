const expect = require('chai').expect;
const firstNonRepeat = require('../firstNonRepeat');

describe('firstNonRepeat', () => {
  it('should return "c" if s ="abacabad"', () => {
    expect(firstNonRepeat('abacabad')).to.equal('c');
  });

  it('should return "_" if s ="abacabaabacaba"', () => {
    expect(firstNonRepeat('abacabaabacaba')).to.equal('_');
  });
});
