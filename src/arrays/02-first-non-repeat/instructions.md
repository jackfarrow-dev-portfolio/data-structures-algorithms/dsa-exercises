# First Non-Repeating Character

> Given a string `s` consisting of small English letters, find and return the
> first instance of a non-repeating character in it. If there is no such
> character, return `_`.

## Example 1

- Input: `s = "abacabad"`
- Output: 'c'
- Explanation: There are 2 non-repeating characters in the string: 'c' and 'd'.
  Return c since it appears in the string first.

## Example 2

- Input: `s = "abacabaabacaba"`
- Output: `_`
- Explanation: There are no characters in this string that do not repeat.
