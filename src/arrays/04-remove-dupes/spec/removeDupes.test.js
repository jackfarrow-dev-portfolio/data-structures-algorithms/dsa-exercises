const expect = require('chai').expect;
const removeDupes = require('../removeDupes');

describe('removeDupes', () => {
  it('should return 2 when nums = [1, 1, 2]', () => {
    const nums = [1, 1, 2];
    expect(removeDupes(nums)).to.equal(2);
  });

  it('should return [1, 2] when nums = [1, 1, 2]', () => {
    const nums = [1, 1, 2];
    removeDupes(nums);
    expect(nums[0]).to.equal(1);
    expect(nums[1]).to.equal(2);
  });

  it('should return 5 when nums = [0, 0, 1, 1, 1, 2, 2, 3, 3, 4]', () => {
    const nums = [0, 0, 1, 1, 1, 2, 2, 3, 3, 4];
    expect(removeDupes(nums)).to.equal(5);
  });

  it('should return [0, 1, 2, 3, 4] when nums = [0, 0, 1, 1, 1, 2, 2, 3, 3, 4]', () => {
    const nums = [0, 0, 1, 1, 1, 2, 2, 3, 3, 4];
    removeDupes(nums);
    expect(nums[0]).to.equal(0);
    expect(nums[1]).to.equal(1);
    expect(nums[2]).to.equal(2);
    expect(nums[3]).to.equal(3);
    expect(nums[4]).to.equal(4);
  });

  it('should return 1 when nums = [1, 1]', () => {
    const nums = [1, 1];
    expect(removeDupes(nums)).to.equal(1);
  });

  it('should return [1] when nums = [1,1]', () => {
    const nums = [1, 1];
    removeDupes(nums);
    expect(nums[0]).to.equal(1);
  });
});
