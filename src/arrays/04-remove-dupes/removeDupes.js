/**
 *
 * @param {number[]} nums
 */
const removeDupes = (nums) => {
  let x = 0;
  let y = 1;
  let answer = nums.length;

  while (y < nums.length) {
    if (nums[x] === nums[y]) {
      nums[y] = Infinity;
      y++;
      answer--;
    } else {
      x = y;
      y++;
    }
  }

  nums.sort((a, b) => a - b);
  return answer;
};

module.exports = removeDupes;
