/**
 *
 * @param {number[]} nums
 * @param {number} k
 * @returns {void} Do not return anything, modify nums in-place
 */
const rotateArray = (nums, k) => {
  k %= nums.length;
  let x = 0;
  let y = nums.length - 1;

  const reverse = (x, y) => {
    while (x < y) {
      let temp = nums[y];
      nums[y] = nums[x];
      nums[x] = temp;
      x++;
      y--;
    }
  };

  reverse(x, y);

  x = 0;
  y = k - 1;

  reverse(x, y);

  x = k;
  y = nums.length - 1;

  reverse(x, y);

  //   let limit = 0;
  //   while (limit < k) {
  //     let shifted = nums.pop();
  //     nums.unshift(shifted);
  //     limit++;
  //   }
};

module.exports = rotateArray;
