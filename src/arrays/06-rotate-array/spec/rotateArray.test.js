const expect = require('chai').expect;
const rotateArray = require('../rotateArray');

describe('rotateArray', () => {
  it('should be [5, 6, 7, 1, 2, 3, 4] if nums = [1, 2, 3, 4, 5, 6, 7] and  k = 3', () => {
    const nums = [1, 2, 3, 4, 5, 6, 7];
    rotateArray(nums, 3);
    expect(nums[0]).to.equal(5);
    expect(nums[1]).to.equal(6);
    expect(nums[2]).to.equal(7);
    expect(nums[3]).to.equal(1);
    expect(nums[4]).to.equal(2);
    expect(nums[5]).to.equal(3);
    expect(nums[6]).to.equal(4);
  });

  it('should be [3, 99, -1, -100] if nums = [-1, -100, 3, 99] and k = 2', () => {
    const nums = [-1, -100, 3, 99];
    rotateArray(nums, 2);
    expect(nums[0]).to.equal(3);
    expect(nums[1]).to.equal(99);
    expect(nums[2]).to.equal(-1);
    expect(nums[3]).to.equal(-100);
  });

  it('should be [2, 1] if nums = [1, 2] and k = 3', () => {
    const nums = [1, 2];
    rotateArray(nums, 3);
    expect(nums[0]).to.equal(2);
    expect(nums[1]).to.equal(1);
  });
});
