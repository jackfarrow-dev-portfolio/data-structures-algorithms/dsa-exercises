/**
 *
 * @param {number[]} digits
 * @returns {number[]}
 */
const plusOne = (digits) => {
  let right = digits.length - 1;
  while (right >= 0) {
    if (digits[right] === 9) {
      digits[right] = 0;
      if (right === 0) {
        digits.unshift(1);
        break;
      } else {
        right--;
      }
    } else {
      digits[right] += 1;
      break;
    }
  }
  return digits;
};

module.exports = plusOne;
