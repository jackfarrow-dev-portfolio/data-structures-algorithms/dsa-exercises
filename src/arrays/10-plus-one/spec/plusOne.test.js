const expect = require('chai').expect;
const plusOne = require('../plusOne');

describe('plusOne', () => {
  it('should return [1, 2, 4] when input = [1, 2, 3]', () => {
    const result = plusOne([1, 2, 3]);
    expect(result[0]).to.equal(1);
    expect(result[1]).to.equal(2);
    expect(result[2]).to.equal(4);
  });

  it('should return [4, 3, 2, 2] when input = [4, 3, 2, 1]', () => {
    const result = plusOne([4, 3, 2, 1]);
    expect(result[0]).to.equal(4);
    expect(result[1]).to.equal(3);
    expect(result[2]).to.equal(2);
    expect(result[3]).to.equal(2);
  });

  it('should return [10] when input = [9]', () => {
    const result = plusOne([9]);
    expect(result[0]).to.equal(1);
    expect(result[1]).to.equal(0);
  });
});
