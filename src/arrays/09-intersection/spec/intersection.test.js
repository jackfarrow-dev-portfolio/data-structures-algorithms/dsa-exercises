const expect = require('chai').expect;
const intersection = require('../intersection');

describe('intersection', () => {
  it('should return [2, 2] when nums1 = [1,2,2,1] and nums2 = [2,2]', () => {
    const nums1 = [1, 2, 2, 1];
    const nums2 = [2, 2];
    const result = intersection(nums1, nums2);
    expect(result[0]).to.equal(2);
    expect(result[1]).to.equal(2);
  });

  it('should return [4, 9] when nums1 = [4, 9, 5] and nums2 = [9, 4, 9, 8, 4]', () => {
    const nums1 = [4, 9, 5];
    const nums2 = [9, 4, 9, 8, 4];
    const result = intersection(nums1, nums2);
    expect(result).to.contains(4);
    expect(result).to.contains(9);
  });
});
