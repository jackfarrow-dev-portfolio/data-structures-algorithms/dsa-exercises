/**
 *
 * @param {number[]} nums1
 * @param {number[]} nums2
 * @returns {number[]}
 */
const intersection = (nums1, nums2) => {
  const result = [];
  const map1 = new Map();
  const map2 = new Map();

  for (const n of nums1) {
    if (!map1.has(n)) {
      map1.set(n, 1);
    } else {
      let count = map1.get(n);
      map1.set(n, count + 1);
    }
  }

  for (const n of nums2) {
    if (!map2.has(n)) {
      map2.set(n, 1);
    } else {
      let count = map2.get(n);
      map2.set(n, count + 1);
    }
  }

  if (map1.size >= map2.size) {
    map2.forEach((val, key) => {
      if (map1.has(key)) {
        let r = new Array(Math.min(val, map1.get(key))).fill(key);
        result.push(...r);
      }
    });
  } else {
    map1.forEach((val, key) => {
      if (map2.has(key)) {
        let r = new Array(Math.min(val, map2.get(key))).fill(key);
        result.push(...r);
      }
    });
  }
  //   console.log(map1);
  //   console.log(map2);
  return result;
};

module.exports = intersection;
