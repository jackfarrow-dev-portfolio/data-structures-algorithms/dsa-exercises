# Rotate a 2D Matrix

> You are given an `n x n` 2D matrix that represents an image. Rotate the image
> by 90 degrees (clockwise). You must solve this task _in-place_, with `O(1)`
> additional memory. Return the rotated matrix.

## Example

- Input: `matrix = [[1, 2, 3], [4, 5, 6], [7, 8, 9]]`
- Output: `[[7, 4, 1], [8, 5, 2], [9, 6, 3]]`

## Example 2

- Input: `matrix = [[1]]`
- Output: `[[1]]`

## Example 3

- Input:
  `matrix = [[10,9,6,3,7], [6,10,2,9,7], [7,6,3,8,2], [8,9,7,9,9], [6,8,6,8,2]]`
- Output: `[[6,8,7,6,10], [8,9,6,10,9], [6,7,3,2,6], [8,9,8,9,3], [2,9,2,7,7]]`
