/**
 *
 * @param {number[][]} matrix
 * @returns {number[][]}
 */
const rotate2dArray = (matrix) => {
  let left = 0;
  let right = matrix.length - 1;

  while (left < right) {
    for (i = 0; i < right - left; i++) {
      let top = left;
      let bottom = right;

      // Save the top-left value
      let topLeft = matrix[top][left + i];

      // Move bottom-left into top-left
      matrix[top][left + i] = matrix[bottom - i][left];

      // Move bottom-right into bottom-left
      matrix[bottom - i][left] = matrix[bottom][right - i];

      // Move top right into bottom-right
      matrix[bottom][right - i] = matrix[top + i][right];

      // Move top-left into top-right
      matrix[top + i][right] = topLeft;
    }
    right -= 1;
    left += 1;
  }
  return matrix;
};
