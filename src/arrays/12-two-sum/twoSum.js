/**
 *
 * @param {number[]} nums
 * @param {number[]} target
 */
const twoSum = (nums, target) => {
  const map = new Map();
  const result = [];
  for (let i = 0; i < nums.length; i++) {
    map.set(nums[i], i);
  }
  nums.sort((a, b) => a - b);
  let left = 0;
  let right = nums.length - 1;

  let currentSum = nums[0];
  while (left <= right) {
    // currentSum += nums[left];
    currentSum += nums[right];

    if (currentSum > target) {
      currentSum -= nums[right];
      right--;
    } else if (currentSum < target) {
      currentSum -= nums[left];
      left++;
    } else {
      break;
    }
    // if (currentSum + nums[right] > target) {
    //   right--;
    // } else if (currentSum + nums[right] < target) {
    //   left++;
    // } else {
    //   break;
    // }
  }
  //   console.log(`left: ${left}, right: ${right}`);
  return [map.get(nums[left]), map.get(nums[right])];
};

module.exports = twoSum;
