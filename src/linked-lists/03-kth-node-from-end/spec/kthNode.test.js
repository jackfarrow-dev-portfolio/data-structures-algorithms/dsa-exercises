const expect = require('chai').expect;
const SinglyLinkedListNode = require('../../SinglyLinkedListNode');
const kthNodeFromEnd = require('../kthNode');

describe('kthNodeFromEnd', () => {
  const tail = new SinglyLinkedListNode(5);
  const four = new SinglyLinkedListNode(4, tail);
  const three = new SinglyLinkedListNode(3, four);
  const two = new SinglyLinkedListNode(2, three);
  const head = new SinglyLinkedListNode(1, two);

  it('should return node 4 when head = 1->2->3->4->5 and k = 2', () => {
    expect(kthNodeFromEnd(head, 2).val).to.equal(4);
  });

  it('should return node 3 when head = 1->2->3->4->5 and k = 3', () => {
    expect(kthNodeFromEnd(head, 3).val).to.equal(3);
  });
});
