const SinglyLinkedListNode = require('../SinglyLinkedListNode');

/**
 *
 * @param {SinglyLinkedListNode} head
 * @param {number} k
 */
const kthNodeFromEnd = (head, k) => {
  let slow = head;
  let fast = head;

  for (let i = 0; i < k; i++) {
    fast = fast.next;
  }

  while (fast && fast.next) {
    fast = fast.next;
    slow = slow.next;
  }

  return slow.next;
};

module.exports = kthNodeFromEnd;
