class SinglyLinkedListNode {
  constructor(val, next) {
    this.val = val !== undefined ? val : 0;
    this.next = next !== undefined ? next : null;
  }
}

module.exports = SinglyLinkedListNode;
