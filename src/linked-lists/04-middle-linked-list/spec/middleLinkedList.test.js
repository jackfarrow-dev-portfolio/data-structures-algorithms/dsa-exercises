const expect = require('chai').expect;
const SinglyLinkedListNode = require('../../SinglyLinkedListNode');
const getMiddleOfLinkedList = require('../middleLinkedList');

describe('getMiddleOfLinkedList', () => {
  const tail = new SinglyLinkedListNode(5);
  const four = new SinglyLinkedListNode(4, tail);
  const three = new SinglyLinkedListNode(3, four);
  const two = new SinglyLinkedListNode(2, three);
  const head = new SinglyLinkedListNode(1, two);

  it('should return node with value 3 when head = 1->2->3->4->5', () => {
    expect(getMiddleOfLinkedList(head)).to.equal(three);
  });

  it('should return node with value 4 when head = 1->2->3->4->5->6', () => {
    const six = new SinglyLinkedListNode(6);
    tail.next = six;
    expect(getMiddleOfLinkedList(head)).to.equal(four);
  });
});
