const SinglyLinkedListNode = require('../SinglyLinkedListNode');

/**
 *
 * @param {SinglyLinkedListNode} head
 * @returns {SinglyLinkedListNode}
 */
const getMiddleOfLinkedList = (head) => {
  let fast = head;
  let slow = head;

  while (fast && fast.next) {
    fast = fast.next.next;
    slow = slow.next;
  }
  return slow;
};

module.exports = getMiddleOfLinkedList;
