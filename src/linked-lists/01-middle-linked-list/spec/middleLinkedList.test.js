const expect = require('chai').expect;
const middleLinkedList = require('../middleLinkedList');
const SinglyLinkedListNode = require('../../SinglyLinkedListNode');

describe('middleLinkedList', () => {
  let head;

  beforeEach(() => {
    const tail = new SinglyLinkedListNode(5);
    const four = new SinglyLinkedListNode(4, tail);
    const three = new SinglyLinkedListNode(3, four);
    const two = new SinglyLinkedListNode(2, three);
    head = new SinglyLinkedListNode(1, two);
  });

  it('should return 3 if the list = 1->2->3->4->5', () => {
    expect(middleLinkedList(head)).to.equal(3);
  });
});
