const SinglyLinkedListNode = require('../SinglyLinkedListNode');

/**
 *
 * @param {SinglyLinkedListNode} head
 * @returns {number}
 */
const middleLinkedList = (head) => {
  let fast = head;
  let slow = head;

  while (fast && fast.next) {
    fast = fast.next.next;
    slow = slow.next;
  }
  return slow.val;
};

module.exports = middleLinkedList;
