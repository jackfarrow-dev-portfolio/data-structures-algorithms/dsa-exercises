const expect = require('chai').expect;
const DoublyLinkedListNode = require('../../DoublyLinkedListNode');
const {
  addToBeginning,
  addToEnd,
  insertAtIndex,
  removeFromEnd,
  removeFromStart,
  getNodeAtIndex,
} = require('../sentinel-nodes');

describe('dll operations with sentinel nodes', () => {
  let head = new DoublyLinkedListNode(-Infinity);
  let tail = new DoublyLinkedListNode(Infinity);

  beforeEach(() => {
    const three = new DoublyLinkedListNode(3, null, tail);
    const two = new DoublyLinkedListNode(2, null, three);
    const one = new DoublyLinkedListNode(1, head, two);

    two.prev = one;
    three.prev = two;
    head.next = one;
    tail.prev = three;
  });

  describe('insertAtIndex', () => {
    it('should insert a node at the expected index 2', () => {
      const value = -999;
      const index = 2;
      insertAtIndex(head, value, index);
      expect(getNodeAtIndex(head, index).val).to.equal(value);
    });
  });

  describe('removeFromStart', () => {
    it('should remove node 1', () => {
      const answer = removeFromStart(head);
      expect(answer).to.be.true;
      expect(getNodeAtIndex(head, 1).val).to.equal(2);
    });
  });

  describe('removeFromEnd', () => {
    it('should remove the penultimate node', () => {
      const answer = removeFromEnd(head);
      expect(answer).to.be.true;
      expect(getNodeAtIndex(head, 3).val).to.equal(Infinity);
    });
  });

  describe('addToBeginning', () => {
    it('should add a node to the beginning of the list', () => {
      addToBeginning(head, 0);
      expect(getNodeAtIndex(head, 1).val).to.equal(0);
    });
  });

  describe('addToEnd', () => {
    it('should add a node to the end of the list', () => {
      const answer = addToEnd(head, 6);
      expect(getNodeAtIndex(head, 4).val).to.equal(6);
      expect(answer).to.be.true;
    });
  });

  describe('getNodeAtIndex', () => {
    it('should return -Infinity when index = 0', () => {
      expect(getNodeAtIndex(head, 0).val).to.equal(head.val);
    });

    it('should return Infinity when index = number of nodes', () => {
      expect(getNodeAtIndex(head, 4).val).to.equal(tail.val);
    });

    it('should return null when index is out of bounds', () => {
      expect(getNodeAtIndex(head, 9)).to.be.null;
    });

    it('should return null when head is null', () => {
      expect(getNodeAtIndex(null)).to.be.null;
    });
  });
});
