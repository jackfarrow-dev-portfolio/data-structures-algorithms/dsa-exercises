const DoublyLinkedListNode = require('../DoublyLinkedListNode');

const requireHead = (head) => {
  if (head === undefined)
    throw new Error('Unsupported argument: "head" node must not be undefined');
};

/**
 *
 * @param {DoublyLinkedListNode} head
 * @param {any} val
 */
const addToBeginning = (head, val) => {
  requireHead(head);
  const newNode = new DoublyLinkedListNode(val);
  if (!head) return newNode;

  const nextNode = head.next;
  newNode.next = nextNode;
  nextNode.prev = newNode;
  newNode.prev = head;
  head.next = newNode;

  return head;
};

/**
 *
 * @param {DoublyLinkedListNode} head
 * @param {any} val
 * @returns {boolean}
 */
const addToEnd = (head, val) => {
  requireHead(head);
  const newNode = new DoublyLinkedListNode(val);
  let current = head;

  while (current) {
    if (!current.next) {
      let prevNode = current.prev;
      prevNode.next = newNode;
      current.prev = newNode;
      newNode.next = current;

      return true;
    }
    current = current.next;
  }
  return false;
};

/**
 * @param {DoublyLinkedListNode} head
 * @returns {boolean}
 */
const removeFromEnd = (head) => {
  requireHead(head);
  let current = head;
  while (current) {
    if (!current.next) {
      let prevNode = current.prev.prev;
      prevNode.next = current;
      return true;
    }
    current = current.next;
  }
  return false;
};

/**
 *
 * @param {DoublyLinkedListNode} head
 */
const removeFromStart = (head) => {
  requireHead(head);
  if (!head.next) return true;
  head.next = head.next.next;
  head.next.prev = head;
  return true;
};

/**
 *
 * @param {DoublyLinkedListNode} head
 * @param {any} value
 * @param {number} index
 */
const insertAtIndex = (head, value, index) => {
  if (index === 0) addToBeginning(head, value);

  const currentNode = getNodeAtIndex(head, index);
  const newNode = new DoublyLinkedListNode(value);

  if (!currentNode) return newNode;
  const prevNode = currentNode.prev;
  prevNode.next = newNode;
  newNode.prev = prevNode;
  newNode.next = currentNode;
  currentNode.prev = newNode;
};

/**
 *
 * @param {DoublyLinkedListNode} head
 * @param {number} index
 * @returns {any}
 */
const getNodeAtIndex = (head, index) => {
  requireHead(head);
  if (head !== null) {
    if (index === 0) return head;

    let current = head.next;
    let counter = 1;
    while (current) {
      if (counter === index) {
        return current;
      }
      current = current.next;
      counter++;
    }
  }

  return null;
};

module.exports = {
  addToBeginning,
  addToEnd,
  insertAtIndex,
  getNodeAtIndex,
  removeFromEnd,
  removeFromStart,
};
