const expect = require('chai').expect;
const nthNodeFromEnd = require('../nthNodeFromEnd');
const SinglyLinkedListNode = require('../../SinglyLinkedListNode');

describe('nthNodeFromEnd', () => {
  it('should return [1, 2, 3, 5] when head = [1, 2, 3, 4, 5] and n = 2', () => {
    const tail = new SinglyLinkedListNode(5);
    const four = new SinglyLinkedListNode(4, tail);
    const three = new SinglyLinkedListNode(3, four);
    const two = new SinglyLinkedListNode(2, three);
    const head = new SinglyLinkedListNode(1, two);

    const result = nthNodeFromEnd(head, 2);
    expect(result.val).to.equal(1);
    expect(result.next.val).to.equal(2);
    expect(result.next.next.val).to.equal(3);
    expect(result.next.next.next.val).to.equal(5);
    expect(result.next.next.next.next).to.be.null;
  });

  it('should return [] when head = [1] and n = 1', () => {
    const head = new SinglyLinkedListNode(1);
    const result = nthNodeFromEnd(head, 1);
    expect(result).to.be.null;
  });

  it('should return [1] when head = [1, 2] an n = 1', () => {
    const tail = new SinglyLinkedListNode(2);
    const head = new SinglyLinkedListNode(1, tail);
    const result = nthNodeFromEnd(head, 1);
    expect(result.val).to.equal(1);
    expect(result.next).to.be.null;
  });
});
