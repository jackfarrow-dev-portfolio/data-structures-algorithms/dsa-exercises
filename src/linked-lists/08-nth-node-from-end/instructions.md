# Remove `n`th Node from End of List

> Given the head of a linked list, remove the nth node from the end of the list
> and return its head.

## Example 1

- Input: head = [1,2,3,4,5], n = 2
- Output: [1,2,3,5]

## Example 2

- Input: head = [1], n = 1
- Output: []

## Example 3

- Input: head = [1,2], n = 1
- Output: [1]

## Solution A: Two-Pass

```js
const removeNthNodeFromEnd = (head, n) => {
  const dummy = new SinglyLinkedListNode(Infinity, head);
  let len = 0;
  let first = head;
  while (first) {
    len++;
    first = first.next;
  }

  len -= n;
  first = dummy;
  while (len > 0) {
    len--;
    first = first.next;
  }
  first.next = first.next.next;
  return dummy.next;
};
```

Given:

- `head` = `[1,2,3,4,5]`
- `n` = `2`

1. Create an auxiliary "dummy" node to point to `head`. This is to handle edge
   cases such as when removing the `head` node, or when `head` is `null`.

2. Create `len` to represent the length of the list. Set its initial value to
   `0`.
3. While `first` is not null, increment `len` by 1. Traverse `first` until it is
   `null`. At the end, `len = 5`.
4. Subtract `n` from `len`: `len-= n` = `len = 5 - 2` = `3`. This gives us the
   **index of the node we wish to delete**.
5. **Reset** `first` to point to our dummy node.
6. While `len > 0`:
   - **Decrement** `len`.
   - Traverse first (`first = first.next`). This advances the pointer to the
     current node like so:
     - `first` = `Infinity` --> 1 --> 2 --> 3 --> 4 --> 5; `len` = 3
     - `first` = 1 --> 2 --> 3 --> 4 --> 5; `len` = 2
     - `first` = 2 --> 3 --> 4 --> 5; `len` = 1
     - `first` = 3 --> 4 --> 5; `len` = 0
7. **Remove the target node by setting `first.next` to `first.next.next`**:
   - `first` = 3 --> <del>4</del> --> 5
8. Return `dummy.next`, which will point to `head`.

## Solution 2: One-Pass

```js
const removeNthNodeFromEnd = (head, n) => {
  const dummy = new SinglyLinkedListNode(Infinity, head);
  let fast = dummy;
  let slow = dummy;

  for (let i = 0; i <= n; i++) {
    fast = fast.next;
  }

  while (fast) {
    fast = fast.next;
    slow = slow.next;
  }

  slow.next = slow.next.next;
  return dummy.next;
};
```

Given:

- `head` = `[1,2,3,4,5]`
- `n` = `2`

1. Create a `dummy` node to handle edge cases (`head` is null, `head` is only
   node in list...)
2. Create **two pointers**, `fast` and `slow`, each pointing to `dummy`.
3. Advance `fast` ahead of `slow` by `n` positions:
   - For (i = 1; i <= n + 1; i++): - `fast = fast.next`

| i   | n   | i <= n? | fast                  |
| --- | --- | ------- | --------------------- |
| 0   | 2   | true    | 1 -> 2 -> 3 -> 4 -> 5 |
| 1   | 2   | true    | 2 -> 3 -> 4 -> 5      |
| 2   | 2   | true    | 3 -> 4 -> 5           |
| 3   | 2   | false   |                       |

4. While `fast !== null`:
   - Advance `fast` one position
   - Advance `slow` one position At the end of the `while` loop, `fast = null`
     and `slow =` 3 -> 4 -> 5
5. **Remove the target node from `slow`**:
   - `slow.next = slow.next.next`
   - `slow` = `3 -> 5`
   - `head` = `1 -> 2 -> 3 -> 5`
6. Return `dummy.next`, equals `head`
