const SinglyLinkedListNode = require('../SinglyLinkedListNode');

/**
 *
 * @param {SinglyLinkedListNode} head
 * @param {number} n
 * @returns {SinglyLinkedListNode}
 */
const nthNodeFromEnd = (head, n) => {
  // Solution 1: Two-Pass
  //   const dummy = new SinglyLinkedListNode(Infinity, head);
  //   let len = 0;
  //   let first = head;
  //   while (first) {
  //     len++;
  //     first = first.next;
  //   }

  //   len -= n;
  //   first = dummy;
  //   while (len > 0) {
  //     len--;
  //     first = first.next;
  //   }
  //   first.next = first.next.next;
  //   return dummy.next;

  // Solution 2: One-Pass
  //   let fast = head;
  //   let slow = head;

  //   for (let i = 0; i < n; i++) {
  //     fast = fast.next;
  //   }

  //   if (fast === null) return head.next;

  //   while (fast.next) {
  //     slow = slow.next;
  //     fast = fast.next;
  //   }

  //   slow.next = slow.next.next;
  //   return head;

  // Solution 2(Better): Two-Pass
  const dummy = new SinglyLinkedListNode(Infinity, head);
  let fast = dummy;
  let slow = dummy;

  for (let i = 0; i <= n; i++) {
    fast = fast.next;
  }

  while (fast) {
    fast = fast.next;
    slow = slow.next;
  }

  slow.next = slow.next.next;
  return dummy.next;
};

module.exports = nthNodeFromEnd;
