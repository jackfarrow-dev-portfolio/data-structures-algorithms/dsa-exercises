const SinglyLinkedListNode = require('../SinglyLinkedListNode');

/**
 *
 * @param {SinglyLinkedListNode} list1
 * @param {SinglyLinkedListNode} list2
 * @returns {SinglyLinkedListNode}
 */
const mergeSortedList = (list1, list2) => {
  let dummy = new SinglyLinkedListNode(-99);
  let prev = dummy;

  while (list1 && list2) {
    if (list1.val <= list2.val) {
      prev.next = list1;
      list1 = list1.next;
    } else {
      prev.next = list2;
      list2 = list2.next;
    }
    prev = prev.next;
  }

  // At least one of list1 and list2 can still have nodes
  // at this point, so connect the non-null list
  // to the end of the merged list
  prev.next = list1 ? list1 : list2;

  return dummy.next;
};

module.exports = mergeSortedList;
