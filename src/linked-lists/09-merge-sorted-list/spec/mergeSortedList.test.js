const expect = require('chai').expect;
const mergeSortedList = require('../mergeSortedList');
const SinglyLinkedListNode = require('../../SinglyLinkedListNode');
describe('mergeSortedList', () => {
  it('should return  [1,1,2,3,4,4] when list1 = [1, 2, 4] and list2 = [1, 3, 4]', () => {
    const tail1 = new SinglyLinkedListNode(4);
    const two = new SinglyLinkedListNode(2, tail1);
    const head1 = new SinglyLinkedListNode(1, two);

    const tail2 = new SinglyLinkedListNode(4);
    const three = new SinglyLinkedListNode(3, tail2);
    const head2 = new SinglyLinkedListNode(1, three);

    const result = mergeSortedList(head1, head2);
    expect(result.val).to.equal(1);
    expect(result.next.val).to.equal(1);
    expect(result.next.next.val).to.equal(2);
    expect(result.next.next.next.val).to.equal(3);
    expect(result.next.next.next.next.val).to.equal(4);
    expect(result.next.next.next.next.next.val).to.equal(4);
  });

  it('should return null when list1 = null and list2 = null', () => {
    expect(mergeSortedList(null, null)).to.be.null;
  });

  it('should return [0] when list1 = [0] and list2 = null', () => {
    const result = mergeSortedList(new SinglyLinkedListNode(0), null);
    expect(result.val).to.equal(0);
    expect(result.next).to.be.null;
  });
});
