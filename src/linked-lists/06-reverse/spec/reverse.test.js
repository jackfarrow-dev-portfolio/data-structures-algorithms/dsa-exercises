const expect = require('chai').expect;
const reverseLinkedList = require('../reverseLinkedList');
const SinglyLinkedListNode = require('../../SinglyLinkedListNode');

describe('reverseLinkedList', () => {
  it('should reverse a linked list', () => {
    const tail = new SinglyLinkedListNode(3);
    const two = new SinglyLinkedListNode(2, tail);
    const one = new SinglyLinkedListNode(1, two);
    const head = new SinglyLinkedListNode(0, one);

    const reversed = reverseLinkedList(head);
    expect(reversed.val).to.equal(tail.val);
    expect(reversed.next.val).to.equal(two.val);
    expect(reversed.next.next.val).to.equal(one.val);
    expect(reversed.next.next.next.val).to.equal(head.val);
    expect(reversed.next.next.next.next).to.equal(null);
  });
});
