# Reverse a Linked List

> Given the `head` of a linked list, return a new list with the nodes in
> **reverse order**.

## Example

- Input: `head = 0 --> 1 --> 2 --> 3`
- Output: `3 --> 2 --> 1 --> 0`

## Solution

```js
/**
 *
 * @param {SinglyLinkedListNode} head
 * @returns {SinglyLinkedListNode}
 */
const reverseLinkedList = (head) => {
  let current = head;
  let prev = null;

  while (current) {
    let next = current.next;
    current.next = prev;
    prev = current;
    current = next;
  }
  return prev;
};
```

| i   | curr             | curr!== null? | next          | prev          | curr.next     | prev                 | curr          |
| --- | ---------------- | ------------- | ------------- | ------------- | ------------- | -------------------- | ------------- |
| 0   | 0->1->2->3->null | true          | 1->2->3->null | null          | null          | 0->null              | 1->2->3->null |
| 1   | 1->2->3->null    | true          | 2->3->null    | 0->null       | 0->null       | 1->0->null           | 2->3->null    |
| 2   | 2->3->null       | true          | 3->null       | 1->0->null    | 1->0->null    | 2->1->0->null        | 3->null       |
| 3   | 3->null          | true          | null          | 2->1->0->null | 2->1->0->null | **3->2->1->0->null** | null          |
| 4   | null             | true          | -             | -             | -             | -                    | -             |
