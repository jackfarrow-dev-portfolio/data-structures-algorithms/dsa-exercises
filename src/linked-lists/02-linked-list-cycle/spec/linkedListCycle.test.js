const expect = require('chai').expect;
const linkedListCycle = require('../linkedListCycle');
const SinglyLinkedListNode = require('../../SinglyLinkedListNode');

describe('linkedListCycle', () => {
  it('should return true if head = [3, 2, 0, -4] and a cycle exists at index 1', () => {
    const tail = new SinglyLinkedListNode(-4);
    const zero = new SinglyLinkedListNode(0, tail);
    const two = new SinglyLinkedListNode(2, zero);
    const head = new SinglyLinkedListNode(3, two);

    tail.next = two;

    expect(linkedListCycle(head)).to.equal(true);
  });

  it('should return true if head = [1, 2] and a cycle exists at index 0', () => {
    const tail = new SinglyLinkedListNode(2);
    const head = new SinglyLinkedListNode(1, tail);
    tail.next = head;
    expect(linkedListCycle(head)).to.equal(true);
  });

  it('should return false if head = [1] and a cycle exists at index 1', () => {
    expect(linkedListCycle(new SinglyLinkedListNode(1))).to.equal(false);
  });
});
