const expect = require('chai').expect;
const isPalindrome = require('../isPalindrome');
const SinglyLinkedListNodeNode = require('../../SinglyLinkedListNode');

describe('isPalindromeLinkedList', () => {
  it('should return true when head = [1, 2, 2, 1]', () => {
    const tail = new SinglyLinkedListNodeNode(1);
    const c = new SinglyLinkedListNodeNode(2, tail);
    const b = new SinglyLinkedListNodeNode(2, c);
    const head = new SinglyLinkedListNodeNode(1, b);
    expect(isPalindrome(head)).to.be.true;
  });

  it('should return false when head = [1, 2]', () => {
    const tail = new SinglyLinkedListNodeNode(2);
    const head = new SinglyLinkedListNodeNode(1, tail);
    expect(isPalindrome(head)).to.be.false;
  });
});
