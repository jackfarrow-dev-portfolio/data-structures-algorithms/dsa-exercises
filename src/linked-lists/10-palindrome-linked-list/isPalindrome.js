const SinglyLinkedListNode = require('../SinglyLinkedListNode');

/**
 *
 * @param {SinglyLinkedListNode} head
 * @returns {boolean}
 */
// THIS SOLUTION WORKS
const isPalindrome = (head) => {
  const result = [];
  let current = head;
  while (current) {
    result.push(current.val);
    current = current.next;
  }

  let left = 0;
  let right = result.length - 1;
  while (left < right) {
    if (result[left] !== result[right]) return false;
    left++;
    right--;
  }
  return true;
};

module.exports = isPalindrome;
