class DoublyLinkedListNode {
  constructor(val, prev, next) {
    this.val = val !== undefined ? val : 0;
    this.prev = prev !== undefined ? prev : null;
    this.next = next !== undefined ? next : null;
  }
}

module.exports = DoublyLinkedListNode;
