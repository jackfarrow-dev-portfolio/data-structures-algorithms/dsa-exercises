const expect = require('chai').expect;
const linkedListCycle = require('../linkedListCycle');
const SinglyLinkedListNode = require('../../SinglyLinkedListNode');

describe('linkedListCycle', () => {
  it('should return true if head = [3,2,0,-4] and pos = 1', () => {
    const two = new SinglyLinkedListNode(2);
    const tail = new SinglyLinkedListNode(-4, two);
    const zero = new SinglyLinkedListNode(0);
    const head = new SinglyLinkedListNode(3, two);
    two.next = zero;
    zero.next = tail;
    expect(linkedListCycle(head)).to.be.true;
  });

  it('should return true if head = [1, 2] and pos = 0', () => {
    const tail = new SinglyLinkedListNode(2);
    const head = new SinglyLinkedListNode(1, tail);
    tail.next = head;
    expect(linkedListCycle(head)).to.be.true;
  });

  it('should return false if head = [1] and pos = -1', () => {
    expect(linkedListCycle(new SinglyLinkedListNode(1))).to.be.false;
  });
});
