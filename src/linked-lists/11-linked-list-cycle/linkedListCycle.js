/**
 *
 * @param {SinglyLinkedListNode} head
 * @returns {boolean}
 */
const linkedListCycle = (head) => {
  if (!head || !head.next) return false;
  let fast = head.next;
  let slow = head;

  while (fast) {
    if (!fast.next) return false;
    if (fast === slow) return true;
    slow = slow.next;
    fast = fast.next.next;
  }
  return false;
};

module.exports = linkedListCycle;
