const expect = require('chai').expect;
const deleteNode = require('../deleteNode');
const SinglyLinkedListNode = require('../../SinglyLinkedListNode');

describe('deleteNode', () => {
  it('should return [4, 1, 9] when head = [4, 5, 1, 9] and node = 5', () => {
    const tail = new SinglyLinkedListNode(9);
    const one = new SinglyLinkedListNode(1, tail);
    const five = new SinglyLinkedListNode(5, one);
    const head = new SinglyLinkedListNode(4, five);

    deleteNode(head, 5);
    expect(head.next.val).to.equal(1);
    expect(head.next.next.val).to.equal(9);
  });

  it('should return [4, 5, 9] when head = [4, 5, 1, 9] and node = 1', () => {
    const tail = new SinglyLinkedListNode(9);
    const one = new SinglyLinkedListNode(1, tail);
    const five = new SinglyLinkedListNode(5, one);
    const head = new SinglyLinkedListNode(4, five);

    deleteNode(head, 1);
    expect(head.next.val).to.equal(5);
    expect(head.next.next.val).to.equal(9);
  });
});
