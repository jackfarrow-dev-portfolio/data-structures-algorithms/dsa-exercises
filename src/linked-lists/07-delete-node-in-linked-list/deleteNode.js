const SinglyLinkedListNode = require('../SinglyLinkedListNode');

/**
 *
 * @param {SinglyLinkedListNode} head
 * @param {number} node
 * @returns {void}
 */
const deleteNode = (head, node) => {
  let current = head;
  while (current && current.next) {
    if (current.next.val === node) {
      current.next = current.next.next;
    } else {
      current = current.next;
    }
  }
};

module.exports = deleteNode;
