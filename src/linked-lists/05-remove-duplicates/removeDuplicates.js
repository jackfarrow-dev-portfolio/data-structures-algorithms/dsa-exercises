const SinglyLinkedListNode = require('../SinglyLinkedListNode');

/**
 *
 * @param {SinglyLinkedListNode} head
 * @returns {SinglyLinkedListNode}
 */
const removeDuplicates = (head) => {
  let current = head;
  let currentVal = current.val;
  while (current && current.next) {
    if (current.next.val === currentVal) {
      current.next = current.next.next;
    } else {
      current = current.next;
    }
  }

  return head;
};

module.exports = removeDuplicates;
