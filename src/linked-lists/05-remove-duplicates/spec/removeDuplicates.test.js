const expect = require('chai').expect;
const SinglyLinkedListNode = require('../../SinglyLinkedListNode');
const removeDuplicates = require('../removeDuplicates');

const buildResultArray = (node) => {
  const result = [];
  while (node) {
    result.push(node.val);
    node = node.next;
  }
  return result;
};

describe('removeDuplicates', () => {
  it('should return [1, 2] if head = [1, 1, 2]', () => {
    const tail = new SinglyLinkedListNode(2);
    const one = new SinglyLinkedListNode(1, tail);
    const head = new SinglyLinkedListNode(1, one);
    const answer = removeDuplicates(head);
    expect(buildResultArray(answer)).to.include.members([1, 2]);
  });

  it('should return [1, 2, 3] if head = [1, 1, 2, 3, 3]', () => {
    const tail = new SinglyLinkedListNode(3);
    const three = new SinglyLinkedListNode(3, tail);
    const two = new SinglyLinkedListNode(2, three);
    const one = new SinglyLinkedListNode(1, two);
    const head = new SinglyLinkedListNode(1, one);
    const answer = removeDuplicates(head);
    expect(buildResultArray(answer)).to.include.members([1, 2, 3]);
  });
});
